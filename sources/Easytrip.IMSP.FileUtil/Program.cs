﻿using Easytrip.IMSP.FileUtil.Config;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace Easytrip.IMSP.FileUtil
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IoC.Initialize(args);

                var logger = IoC.Container.GetRequiredService<ILogger<Program>>();

                var runType = IoC.Container.GetService<IConfiguration>().GetValue<string>("run-type");
                if ("CEI".Equals(runType, StringComparison.OrdinalIgnoreCase))
                {
                    var fp = IoC.Container.GetRequiredService<IFileProcessor>("CEI");
                    fp.ProcessFiles();
                }
                else
                {
                    try
                    {
                        var fp = IoC.Container.GetRequiredService<IFileProcessor>("IMSP_Out");
                        fp.ProcessFiles();
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, "Problem processing IMSP_Out");
                    }

                    try
                    {
                        var fp2 = IoC.Container.GetRequiredService<IFileProcessor>("IMSP_In");
                        fp2.ProcessFiles();
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, "Problem processing IMSP_In");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unknown error occurred: " + ex.Message);
                Console.WriteLine(ex.StackTrace);

                //if startup is failing, the only hope to log something is by writing to serilog directly.
                if (Serilog.Log.Logger != null) Serilog.Log.Logger.Error(ex, "Problem moving files");
            }
        }
    }
}