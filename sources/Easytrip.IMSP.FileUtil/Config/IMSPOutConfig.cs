﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easytrip.IMSP.FileUtil.Config
{
    public class IMSPOutConfig
    {
        public SourceInfo Source { get; set; }
        public string DestinationPath { get; set; }
        public string CEIFile { get; set; }
        public string OnCompletedScript { get; set; }
    }
}