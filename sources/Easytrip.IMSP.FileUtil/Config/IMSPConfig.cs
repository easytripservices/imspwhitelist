﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easytrip.IMSP.FileUtil.Config
{
    public class IMSPConfig
    {
        public string CO { get; set; }
        public IMSPInConfig In { get; set; }
        public IMSPOutConfig Out { get; set; }
    }
}