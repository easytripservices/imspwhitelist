﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easytrip.IMSP.FileUtil.Config
{
    public class IMSPInConfig
    {
        public string Source { get; set; }
        public string Archive { get; set; }
        public SourceInfo DestinationPath { get; set; }
    }
}