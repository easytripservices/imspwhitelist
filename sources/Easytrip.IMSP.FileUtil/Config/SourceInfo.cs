﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easytrip.IMSP.FileUtil.Config
{
    public class SourceInfo
    {
        public string Bucket { get; set; }
        public string Prefix { get; set; }
    }
}
