﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easytrip.IMSP.FileUtil.Config
{
    public class AWSConfig
    {
        public string AccessKey { get; set; }
        public string Secret { get; set; }
    }
}
