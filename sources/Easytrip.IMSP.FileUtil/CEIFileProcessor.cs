﻿using Easytrip.IMSP.FileUtil.Config;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using System.IO;

namespace Easytrip.IMSP.FileUtil
{
    public class CEIFileProcessor : IFileProcessor
    {
        private ILogger<CEIFileProcessor> _logger;
        private IMSPConfig _config;

        public CEIFileProcessor(ILogger<CEIFileProcessor> logger, IMSPConfig config)
        {
            _logger = logger;
            _config = config;
        }

        public void ProcessFiles()
        {
            try
            {
                _logger.LogInformation($"About to generate empty CEI file for CO '{_config.CO}'");

                CopyCEIFile(_config);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Problem processing CO '{_config.CO}'");
            }
        }

        private void CopyCEIFile(IMSPConfig config)
        {
            //this is a IMSP quirk where it requires an empty CEI file to be posted twice a day, at 0920 and 1520

            if (File.Exists(config.Out.CEIFile))
            {
                if (Directory.Exists(config.Out.DestinationPath) == false) Directory.CreateDirectory(config.Out.DestinationPath);

                var destPath = Path.Combine(config.Out.DestinationPath, $"{DateTime.Now.ToString("yyyyMMddHHmmss")}-CEI.XML");

                _logger.LogDebug($"About to copy CEI file to '{destPath}'");

                File.Copy(config.Out.CEIFile, destPath);

                _logger.LogInformation($"Copied CEI file to '{destPath}'");
            }
            else
            {
                _logger.LogInformation($"CEI file not found in {config.Out.CEIFile}");
            }
        }
    }
}