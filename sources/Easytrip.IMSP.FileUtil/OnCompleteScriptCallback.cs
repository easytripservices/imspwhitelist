﻿using Easytrip.IMSP.FileUtil.Config;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Easytrip.IMSP.FileUtil
{
    public class OnCompleteScriptCallback
    {
        private IMSPConfig _config;
        private ILogger<IMSPOutConfig> _logger;

        public OnCompleteScriptCallback(IMSPConfig config, ILogger<IMSPOutConfig> logger)
        {
            _config = config;
            _logger = logger;
        }

        public void Callback()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(_config.Out.OnCompletedScript)) return;

                var proc = Process.Start(_config.Out.OnCompletedScript);
                var result = proc.WaitForExit(60000);
                _logger.LogInformation($"Callback script executed successfully: {result}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Empty);
            }
        }
    }
}