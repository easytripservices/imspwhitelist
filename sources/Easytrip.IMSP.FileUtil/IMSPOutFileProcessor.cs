﻿using System.Linq;
using Amazon.S3.Model;
using Easytrip.IMSP.FileUtil.Config;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Amazon.S3;
using Amazon;
using Amazon.Runtime;
using System.Threading.Tasks;

namespace Easytrip.IMSP.FileUtil
{
    /// <summary>
    /// used to process outgoing files, e.g. white/black/grey lists
    /// </summary>
    public class IMSPOutFileProcessor : IFileProcessor
    {
        private ILogger<IMSPOutFileProcessor> _logger;
        private IMSPConfig _config;
        private AWSConfig _awsConfig;
        private OnCompleteScriptCallback _callback;

        public IMSPOutFileProcessor(ILogger<IMSPOutFileProcessor> logger, Config.IMSPConfig config, AWSConfig awsConfig, OnCompleteScriptCallback callback)
        {
            _logger = logger;
            _config = config;
            _awsConfig = awsConfig;
            _callback = callback;
        }

        public void ProcessFiles()
        {
            try
            {
                _logger.LogInformation($"Retrieving IMSP list files for CO '{_config.CO}'");

                using (var client = new AmazonS3Client(new BasicAWSCredentials(_awsConfig.AccessKey, _awsConfig.Secret), RegionEndpoint.EUWest1))
                {
                    var listTaskReq = client.ListObjectsV2Async(new ListObjectsV2Request()
                    {
                        Prefix = $"{_config.CO}/ToSend",
                        BucketName = _config.Out.Source.Bucket
                    });

                    listTaskReq.Wait();

                    if (listTaskReq.Result.HttpStatusCode != System.Net.HttpStatusCode.OK)
                    {
                        _logger.LogInformation($"Unexpected response for bucket {_config.Out.Source.Bucket}: {listTaskReq.Result.HttpStatusCode}");
                    }

                    TransferFile(listTaskReq, client, "BL");
                    TransferFile(listTaskReq, client, "WL");
                    TransferFile(listTaskReq, client, "GL");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Problem processing CO '{_config.CO}'");
            }

            _callback.Callback();
        }

        private void TransferFile(Task<ListObjectsV2Response> listTaskReq, AmazonS3Client client, string filePostfix)
        {
            var filesToProcess = listTaskReq.Result.S3Objects
                .Where(w => w.Key.EndsWith($"-{filePostfix}.xml", StringComparison.OrdinalIgnoreCase))
                .OrderByDescending(w => w.LastModified);

            var file = filesToProcess.FirstOrDefault();
            if (file == null)
            {
                _logger.LogInformation($"File  with postfix '{filePostfix}' not found for CO '{_config.CO}'");
                return;
            }

            CopyFileFromS3(file, _config.Out, client);
            DeleteFilesFromS3(filesToProcess, client, _config.Out);
        }

        private void DeleteFilesFromS3(IEnumerable<S3Object> filesToDelete, AmazonS3Client client, IMSPOutConfig config)
        {
            var deleteTask = client.DeleteObjectsAsync(new DeleteObjectsRequest
            {
                BucketName = config.Source.Bucket,
                Objects = filesToDelete.Select(s => new KeyVersion {Key = s.Key}).ToList()
            });
            deleteTask.Wait(); //throws exception if delete is failed

            if (deleteTask.IsCompleted == false || deleteTask.Result.HttpStatusCode != System.Net.HttpStatusCode.OK) //when deleting is successful, we don't get a 200 but a 204
            {
                _logger.LogError($"Problem deleting S3 objects from bucket {config.Source.Bucket}");
            }
        }

        private void CopyFileFromS3(S3Object file, IMSPOutConfig config, AmazonS3Client client)
        {
            byte[] data = ReadS3Object(file, client);

            if (Directory.Exists(config.DestinationPath) == false) Directory.CreateDirectory(config.DestinationPath);

            File.WriteAllBytes(Path.Combine(config.DestinationPath, Path.GetFileName(file.Key)), data);
        }

        private byte[] ReadS3Object(S3Object fileToUpload, AmazonS3Client client)
        {
            var getFileResultTask = client.GetObjectAsync(new GetObjectRequest
            {
                BucketName = fileToUpload.BucketName,
                Key = fileToUpload.Key
            });

            getFileResultTask.Wait();

            if (getFileResultTask.Result.HttpStatusCode != System.Net.HttpStatusCode.OK)
            {
                _logger.LogInformation($"Unexpected response for bucket {fileToUpload.BucketName}: {getFileResultTask.Result.HttpStatusCode}");
            }

            var data = new byte[fileToUpload.Size];
            using (var str = getFileResultTask.Result.ResponseStream)
            {
                int currentRead = 0;
                while (currentRead < str.Length)
                {
                    var readTask = str.ReadAsync(data, currentRead, data.Length - currentRead);
                    readTask.Wait();

                    if (readTask.IsCompleted == false) throw new Exception($"Problem reading object '{fileToUpload.Key}'");

                    currentRead += readTask.Result;
                }
            }

            return data;
        }
    }
}