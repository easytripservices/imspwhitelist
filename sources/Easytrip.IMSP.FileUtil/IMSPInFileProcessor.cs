﻿using System.Linq;
using Amazon.S3.Model;
using Easytrip.IMSP.FileUtil.Config;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Amazon.S3;
using Amazon;
using Amazon.Runtime;
using System.Threading.Tasks;
using System.Runtime.InteropServices.ComTypes;

namespace Easytrip.IMSP.FileUtil
{
    /// <summary>
    /// used to process incoming files, e.g. transactions
    /// </summary>
    public class IMSPInFileProcessor : IFileProcessor
    {
        private ILogger<IMSPInFileProcessor> _logger;
        private IMSPConfig _config;
        private AWSConfig _awsConfig;

        public IMSPInFileProcessor(ILogger<IMSPInFileProcessor> logger, Config.IMSPConfig config, AWSConfig awsConfig)
        {
            _logger = logger;
            _config = config;
            _awsConfig = awsConfig;
        }

        public void ProcessFiles()
        {
            try
            {
                //check and create archive directory if it's not there yet
                if (Directory.Exists(_config.In.Archive) == false) Directory.CreateDirectory(_config.In.Archive);

                _logger.LogInformation($"Retrieving IMSP transaction files for CO '{_config.CO}'");

                var filesToUpload = GetFiles(_config.In.Source);
                foreach (var f in filesToUpload)
                {
                    try
                    {
                        UploadFile(f, _config.In.DestinationPath);
                        
                        MoveToArchive(f);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"Problem uploading file '{f}'");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Problem processing CO '{_config.CO}'");
            }
        }

        private void MoveToArchive(string f)
        {
            var newFilePath = Path.Combine(_config.In.Archive, ConstructObjectKey(f));
            if (File.Exists(newFilePath)) newFilePath = Path.Combine(_config.In.Archive, Guid.NewGuid().ToString() + "__" + ConstructObjectKey(f));

            File.Move(f, newFilePath);
        }

        private void UploadFile(string f, SourceInfo destinationPath)
        {
            _logger.LogInformation($"Uploading file '{f}'");

            using (var client = new AmazonS3Client(new BasicAWSCredentials(_awsConfig.AccessKey, _awsConfig.Secret), RegionEndpoint.EUWest1))
            {
                var req = client.PutObjectAsync(new PutObjectRequest
                {
                    BucketName = destinationPath.Bucket,
                    FilePath = f,
                    Key = ConstructObjectKey(f)
                });

                req.Wait();

                if (req.Result.HttpStatusCode != System.Net.HttpStatusCode.OK)
                {
                    _logger.LogInformation($"Unexpected response for bucket {_config.In.DestinationPath.Bucket}: {req.Result.HttpStatusCode}");
                }
                else
                {
                    _logger.LogInformation($"File '{f}' uploaded successfully");
                }
            }
        }

        private string ConstructObjectKey(string f)
        {
            //the object key should be in the following format: IEA.1.yyyyMMddHHmmss-CCEI.xml or IEA.51.yyyyMMddHHmmss-CCEI.xml
            var fn = Path.GetFileName(f); //the incoming files are in the following format: 20200901123030-CCEI.xml
            return $"IEA.{_config.CO}.{fn}";
        }

        private IEnumerable<string> GetFiles(string source)
        {
            var files = Directory
                .GetFiles(source)
                .Where(w => w.EndsWith("-CCEI.xml"));

            return files;
        }
    }
}