﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easytrip.IMSP.FileUtil
{
    public interface IFileProcessor
    {
        void ProcessFiles();
    }
}