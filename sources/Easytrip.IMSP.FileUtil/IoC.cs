﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Easytrip.IMSP.FileUtil.Config;
using System.IO;
using Serilog;
using System.Reflection;

namespace Easytrip.IMSP.FileUtil
{
    public static class IoC
    {
        public static ServiceProvider Container { get; private set; }

        public static void Initialize(string[] args)
        {
            var services = ConfigureServices(args);
            Container = services.BuildServiceProvider();
        }

        private static IServiceCollection ConfigureServices(string[] args)
        {
            var services = new ServiceCollection();

            LoadConfiguration(services, args);

            Log.Logger = new LoggerConfiguration()
                .WriteTo.RollingFile("logs\\imsp-{Date}.txt")
                .CreateLogger();
            
            services.AddLogging(opt =>
                opt
                    .AddSerilog()
                    .AddConsole()
                    .AddDebug());

            services.AddTransient<IFileProcessor, CEIFileProcessor>("CEI");
            services.AddTransient<IFileProcessor, IMSPOutFileProcessor>("IMSP_Out");
            services.AddTransient<IFileProcessor, IMSPInFileProcessor>("IMSP_In");
            
            return services;
        }

        private static void LoadConfiguration(ServiceCollection services, string[] args)
        {
            var root = GetRootDirectory();
            Directory.SetCurrentDirectory(root);

            var cb = new ConfigurationBuilder();
            cb.SetBasePath(Directory.GetCurrentDirectory());
            cb.AddJsonFile("appsettings.json", false, true);
            cb.AddCommandLine(args);

            var config = cb.Build();
            services.AddSingleton<IConfiguration>(config);

            var imspConfig = config.GetSection("IMSP").Get<IMSPConfig>();
            services.AddSingleton(imspConfig);

            var awsConfig = config.GetSection("AWS").Get<AWSConfig>();
            services.AddSingleton(awsConfig);

            services.AddSingleton<OnCompleteScriptCallback>();
        }

        private static string GetRootDirectory()
        {
            var assLoc = Assembly.GetExecutingAssembly().Location;
            return Path.GetDirectoryName(assLoc);
        }
    }
}