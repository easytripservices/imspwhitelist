﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Facades.Tests
{
    [TestClass]
    public class SFTPPathTest
    {
        [TestMethod]
        public void TestPath()
        {
            var result = SFTPPath.Join("asf/asd", "asd");
            Assert.AreEqual("asf/asd/asd", result);

            result = SFTPPath.Join("asf/asd/", "asd");
            Assert.AreEqual("asf/asd/asd", result);

            result = SFTPPath.Join("asf///asd", "asd");
            Assert.AreEqual("asf/asd/asd", result);

            result = SFTPPath.Join("asf/asd//", "/asd");
            Assert.AreEqual("asf/asd/asd", result);
        }
    }
}