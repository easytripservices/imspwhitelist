﻿using Amazon.S3;
using Amazon.S3.Model;
using Easytrip.IMSP.Common.Core.Config;
using Easytrip.IMSP.Common.Core.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Facades.Tests
{
    [TestClass]
    public class BillingFacadeTest
    {
        private Mock<IAmazonS3> _s3ClientMock;
        private ServiceProvider _provider;
        private Mock<IXMLHelper> _xmlHelperMock;
        private BillingConfig _config;

        [TestInitialize]
        public void Init()
        {
            var services = new ServiceCollection();

            _config = new BillingConfig()
            {
                ErrorBucket = "errorBucket",
                SuccessBucket = "successBucket",
                XSD = new BillingConfig.XsdConfig
                {
                    Bucket = "bucket",
                    FilePath = "path"
                }
            };
            services.AddSingleton(_config);
            services.AddLogging();
            services.AddTransient<BillingFacade>();

            _xmlHelperMock = new Mock<IXMLHelper>();
            services.AddSingleton(_xmlHelperMock.Object);

            _s3ClientMock = new Mock<IAmazonS3>();
            _s3ClientMock
                .Setup(s => s.PutObjectAsync(It.IsAny<PutObjectRequest>(), default))
                .Returns(Task.FromResult(new PutObjectResponse
                {
                    HttpStatusCode = System.Net.HttpStatusCode.OK
                }));
            services.AddSingleton(_s3ClientMock.Object);

            _provider = services.BuildServiceProvider();
        }

        [TestMethod]
        public async Task TestValidateFile_XSDBucketSet()
        {
            _xmlHelperMock
                .Setup(s => s.PerformXsdValidation(It.IsAny<IAmazonS3>(), It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            var facade = _provider.GetRequiredService<BillingFacade>();
            var result = await facade.ValidateFile(new MemoryStream());
            Assert.IsTrue(result);

            _xmlHelperMock
                .Verify(v => v.PerformXsdValidation(It.IsAny<IAmazonS3>(), It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once());
        }

        [TestMethod]
        public async Task TestValidateFile_XSDBucketNotSet()
        {
            _config.XSD.Bucket = "";
            _config.XSD.FilePath = "";

            var facade = _provider.GetRequiredService<BillingFacade>();
            var result = await facade.ValidateFile(new MemoryStream());
            Assert.IsTrue(result);

            _xmlHelperMock
                .Verify(v => v.PerformXsdValidation(It.IsAny<IAmazonS3>(), It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never());
        }

        [DataRow(true)]
        [DataRow(false)]
        [DataTestMethod]
        public async Task UploadCCEI(bool passes)
        {
            _xmlHelperMock
                .Setup(s => s.PerformXsdValidation(It.IsAny<IAmazonS3>(), It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(passes));

            var facade = _provider.GetRequiredService<BillingFacade>();
            await facade.UploadCCEI(new MemoryStream(), 1, "file");

            var bucket = "errorBucket";
            if (passes) bucket = "successBucket";

            _s3ClientMock
                .Verify(v => v.PutObjectAsync(It.Is<PutObjectRequest>(i => i.BucketName == bucket), default));
        }
    }
}