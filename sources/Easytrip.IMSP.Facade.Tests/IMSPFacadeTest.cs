using Easytrip.IMSP.AWS;
using Easytrip.IMSP.Common.Core.Config;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace Easytrip.IMSP.Facades.Tests
{
    [Ignore]
    [TestClass]
    public class IMSPFacadeTest
    {
        private ServiceProvider _provider;
        private IIMSPFacade _facade;
        private IMSPConfig _impConfig;
        private const string DEFAULT_PATH = "/var/log/";
        private const int DEFAULT_CO = 01;

        [TestInitialize]
        public void Init()
        {
            var services = new ServiceCollection();
            services.AddIMSPFacades();
            services.AddLogging();

            var config = new ConfigurationBuilder()
                .AddInMemoryCollection()
                .Build();
            config["AWS:AccessKey"] = "AKIA477I5JUCJ6MC6TFA";
            config["AWS:Secret"] = "TODO";
            services.AddSingleton<IConfiguration>(config);

            _impConfig = new IMSPConfig
            {
                SFTP = new SFTPConfig
                {
                    Host = "63.35.183.178",
                    SecretName = "imp-sftp-key-devops",
                    Username = "coclient",
                    COPortMapping=new System.Collections.Generic.Dictionary<string, int>
                    {
                        {"01", 2222 }
                    }
                }
            };
            services.AddSingleton(_impConfig);

            services.AddAWSClients();

            _provider = services.BuildServiceProvider();
            _facade = _provider.GetRequiredService<IIMSPFacade>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _provider.Dispose();
        }

        [TestMethod]
        public void TestListIncomingFiles()
        {
            var files = _facade.ListIncomingFiles(DEFAULT_CO, "/");
            Assert.IsTrue(files.Count() > 0);
        }

        [TestMethod]
        public void TestGetFile()
        {
            //NOTE: for this test to pass a file test.txt must be present on the file system
            var files = _facade
                .ListIncomingFiles(DEFAULT_CO, DEFAULT_PATH)
                .Where(w => w.Equals("test.txt"));

            using var stream = _facade.GetFile(DEFAULT_CO, SFTPPath.Join(DEFAULT_PATH, files.First()));
            using (var ms = new MemoryStream())
            {
                stream.CopyTo(ms);

                Assert.IsTrue(ms.Length > 0);
            }
        }

        [TestMethod]
        public void TestSendFile()
        {
            var files = _facade.ListIncomingFiles(DEFAULT_CO, DEFAULT_PATH);
            var fileName = Guid.NewGuid().ToString();
            var data = Guid.NewGuid().ToString();

            using (var ms = new MemoryStream())
            using (var sw = new StreamWriter(ms))
            {
                sw.Write(data);
                sw.Flush();
                ms.Position = 0;

                _facade.SendFile(DEFAULT_CO, ms, SFTPPath.Join(DEFAULT_PATH, fileName));
            }

            var files2 = _facade.ListIncomingFiles(DEFAULT_CO, DEFAULT_PATH);
            Assert.AreEqual(files.Count() + 1, files2.Count());

            using var uploadedFile = _facade.GetFile(DEFAULT_CO, SFTPPath.Join(DEFAULT_PATH, fileName));
            using var sr = new StreamReader(uploadedFile);
            var data2 = sr.ReadToEnd();
            Assert.AreEqual(data, data2);
        }

        [TestMethod]
        public void TestWithMultipleCOClientsSimultaneously()
        {
            throw new NotImplementedException();
        }
    }
}