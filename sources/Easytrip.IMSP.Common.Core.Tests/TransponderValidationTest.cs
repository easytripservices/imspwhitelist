using System .Linq;
using Easytrip.IMSP.Common.Core.Utils;
using Easytrip.IMSP.Common.Core.Validations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Easytrip.IMSP.Common.Core.Tests
{
    [TestClass]
    public class TransponderValidationTest
    {
        [TestMethod]
        public void TestHexAlias()
        {
            var result = TransponderValidation.IsVehicleTransponderValid(new Domain.VehicleTransponderResponseModel
            {
                HexAlias = "asdfzxcv"
            });

            Assert.IsFalse(result);

            result = TransponderValidation.IsVehicleTransponderValid(new Domain.VehicleTransponderResponseModel
            {
                HexAlias = "123adf"
            });

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestTollClass()
        {
            var result = TransponderValidation.IsVehicleTransponderValid(new Domain.VehicleTransponderResponseModel
            {
                HexAlias = "123adf"
            });
            Assert.IsTrue(result);

            for (int i = 1; i < 9; i++)
            {
                result = TransponderValidation.IsVehicleTransponderValid(new Domain.VehicleTransponderResponseModel
                {
                    TollingClass = i,
                    HexAlias = "123adf"
                });
                Assert.IsTrue(result);
            }

            result = TransponderValidation.IsVehicleTransponderValid(new Domain.VehicleTransponderResponseModel
            {
                TollingClass = 0,
                HexAlias = "123adf"
            });
            Assert.IsFalse(result);

            result = TransponderValidation.IsVehicleTransponderValid(new Domain.VehicleTransponderResponseModel
            {
                TollingClass = 10,
                HexAlias = "123adf"
            });
            Assert.IsFalse(result);
        }
    }
}