﻿using Amazon.S3;
using Easytrip.IMSP.Common.Core.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Common.Core.Tests.Utils
{
    [TestClass]
    public class XMLHelperTest
    {
        private Mock<IAmazonS3> _s3Mock;

        [TestInitialize]
        public void Init()
        {
            _s3Mock = new Mock<IAmazonS3>();
        }

        [TestMethod]
        public void WriteXmlContentsTest()
        {
            var writer = new XMLHelper();
            var result = writer.WriteXmlContents(
                99,
                new List<Domain.VehicleTransponderModel>
                {
                    new Domain.VehicleTransponderModel
                    {
                        ContextMark="123123123",
                        ExpiryDate= DateTime.Now,
                        HexAlias="12345",
                        Id=123,
                        VehicleClass=1,
                        VehicleRegistration="reg",
                        Reason="reason"
                    }
                },
                DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"),
                ListTypes.White);

            Assert.IsTrue(result.Position > 0);//this ensures the stream is not disposed
        }

        [TestMethod]
        public async Task PerformXsdValidationTest()
        {
            _s3Mock
                .Setup(s => s.GetObjectAsync(It.IsAny<string>(), It.IsAny<string>(), default(CancellationToken)))
                .Returns<string, string, CancellationToken>((s1, s2, s3) =>
                {
                    return Task.FromResult(new Amazon.S3.Model.GetObjectResponse
                    {
                        ResponseStream = File.OpenRead("Resources\\WhiteListSchema.xml")
                    });
                });

            var writer = new XMLHelper();

            var stream = writer.WriteXmlContents(
               99,
               new List<Domain.VehicleTransponderModel>
               {
                    new Domain.VehicleTransponderModel
                    {
                        ContextMark="123123123",
                        ExpiryDate= DateTime.Now,
                        HexAlias="12345",
                        Id=123,
                        VehicleClass=1,
                        VehicleRegistration="reg",
                        Reason="reason"
                    }
               },
               DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"),
               ListTypes.White);

            var result=await writer.PerformXsdValidation(
                _s3Mock.Object,
                stream,
                "someBucket",
                "bucketPath");

            Assert.IsFalse(result);
            Assert.IsTrue(stream.Position > 0);

            //case 2: valid row
            stream = writer.WriteXmlContents(
              1,
              new List<Domain.VehicleTransponderModel>
              {
                    new Domain.VehicleTransponderModel
                    {
                        ContextMark="64000D000102",
                        ExpiryDate= DateTime.Now,
                        HexAlias="0000A7B0",
                        Id=0000042928,
                        VehicleClass=1,
                        VehicleRegistration="reg",
                        Reason="reason"
                    }
              },
              DateTime.Now.ToString("yyyyMMddHHmmss"),
              ListTypes.White);

             result = await writer.PerformXsdValidation(
                _s3Mock.Object,
                stream,
                "someBucket",
                "bucketPath");

            Assert.IsTrue(result);
            Assert.IsTrue(stream.Position > 0);
        }
    }
}