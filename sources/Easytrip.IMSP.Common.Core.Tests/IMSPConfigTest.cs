﻿using Easytrip.IMSP.Common.Core.Config;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Easytrip.IMSP.Common.Core.Tests
{
    [TestClass]
    public class IMSPConfigTest
    {
        [TestMethod]
        public void TestCoMappingIsDeserializedIntoDictionary()
        {
            var config = new ConfigurationBuilder()
                .AddInMemoryCollection()
                .Build();

            config["IMSP:SFTP:COPortMapping:01"] = "1";
            config["IMSP:SFTP:COPortMapping:51"] = "2";
            config["IMSP:SFTP:COPortMapping:97"] = "30";

            var ic = config
                .GetSection("IMSP")
                .Get<IMSPConfig>();
            Assert.AreEqual(3, ic.SFTP.COPortMapping.Count);
            Assert.AreEqual(1, ic.SFTP.COPortMapping["01"]);
            Assert.AreEqual(2, ic.SFTP.COPortMapping["51"]);
            Assert.AreEqual(30, ic.SFTP.COPortMapping["97"]);
        }
    }
}