﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.ListTransfer.Lambda.Models
{
    public class CCEIProcessingParams: IActionParams
    {
        public int CO { get; set; }
    }
}