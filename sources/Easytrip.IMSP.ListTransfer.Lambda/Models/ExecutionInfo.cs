﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Easytrip.IMSP.ListTransfer.Lambda.Models
{
    public class ExecutionInfo
    {

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public ActionTypes Action { get; set; }

        public JsonElement ActionParams { get; set; }

        public TParams GetActionParams<TParams>() where TParams : IActionParams
        {
            var result = ActionParams.Deserialize<TParams>(new JsonSerializerOptions(JsonSerializerDefaults.Web));
            return result;
        }
    }
}