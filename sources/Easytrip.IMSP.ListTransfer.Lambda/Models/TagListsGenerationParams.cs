﻿using Easytrip.IMSP.Common.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Easytrip.IMSP.ListTransfer.Lambda.Models
{
    public class TagListsGenerationParams : IActionParams
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public ListTypes ListType { get; set; }
    }
}