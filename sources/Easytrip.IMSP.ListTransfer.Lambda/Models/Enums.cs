﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.ListTransfer.Lambda.Models
{
    public enum ActionTypes : byte
    {
        GenerateTagLists_New = 2,
        MoveCCEI = 3
    }
}