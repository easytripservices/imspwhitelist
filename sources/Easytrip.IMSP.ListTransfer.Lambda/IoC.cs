﻿using Easytrip.IMSP.AWS;
using Easytrip.IMSP.Common.Core;
using Easytrip.IMSP.Facades;
using Easytrip.IMSP.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Easytrip.IMSP.ListTransfer.Lambda
{
    public static class IoC
    {
        public static ServiceProvider Container { get; internal set; }

        public static void Initialize(Action<ServiceCollection> serviceRegistrations = null)
        {
            if (Container != null) return;

            ServiceCollection services = new ServiceCollection();
            services.AddAWSClients();
            services.AddIMSPServices();
            services.AddIMSPFacades();
            services.AddCommonUtils();

            services.AddSingleton<IConfiguration>(sp =>
            {
                var configPath = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
                var config = new ConfigurationBuilder()
                    .AddJsonFile(configPath, true)
                    .AddEnvironmentVariables()
                    .Build();
                return config;
            });

            services
                .AddLogging(c =>
                    c.AddLambdaLogger(new LambdaLoggerOptions()
                    {
                        IncludeException = true,
                        IncludeEventId = true,
                        IncludeLogLevel = true,
                        IncludeNewline = true,
                        IncludeCategory = true,
                        IncludeScopes = true
                    }));

            if (serviceRegistrations != null) serviceRegistrations(services);

            Container = services.BuildServiceProvider();
        }

        public static void Dispose()
        {
            Container.Dispose();
            Container = null;
        }
    }
}