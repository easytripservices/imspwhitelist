using Amazon.Lambda.Core;
using Amazon.Lambda.SNSEvents;
using Easytrip.IMSP.Common.Core.Domain;
using Easytrip.IMSP.Common.Core.Utils;
using Easytrip.IMSP.Facades;
using Easytrip.IMSP.ListTransfer.Lambda.Models;
using Easytrip.IMSP.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Text.Json;


// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace Easytrip.IMSP.ListTransfer.Lambda;

/*
 This function is designed to be executed by various EventBridge rules, depending on the frequency and type of command.
 The following commands are supported: 
    *generate W/B/G list files the old way. 
    *generate W/B/G list files the new way (i.e. produce ZIP file with checksum). 
    *move incoming CCEI files to billing
 */

public class Function
{
    private ILogger<Function> _logger;
    private IServiceInventoryFacade _serviceInventoryFacade;
    private ITagListGenerator _newFileGenerator;
    private ICOProcessor _coProcessor;

    public Function()
    {
        IoC.Initialize();

        _logger = IoC.Container.GetRequiredService<ILogger<Function>>();
        _serviceInventoryFacade = IoC.Container.GetRequiredService<IServiceInventoryFacade>();
        _newFileGenerator = IoC.Container.GetRequiredService<ITagListGenerator>();
        _coProcessor = IoC.Container.GetRequiredService<ICOProcessor>();
    }

    /// <summary>
    /// The argument to this function would be the data passed from the EventBridge rule. 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="context"></param>
    /// <returns></returns>
    public async Task FunctionHandler(ExecutionInfo data, ILambdaContext context)
    {
        _logger.LogInformation($"Executing action: '{data.Action}'");

        switch (data.Action)
        {
            case ActionTypes.GenerateTagLists_New:
                await GenerateTagListFile(_newFileGenerator, data.GetActionParams<TagListsGenerationParams>());
                break;

            case ActionTypes.MoveCCEI:
                await ProcessCCEI(data.GetActionParams<CCEIProcessingParams>());
                break;

            default: throw new ArgumentOutOfRangeException(nameof(ExecutionInfo.Action));
        }

        await Task.CompletedTask;
    }

    private async Task ProcessCCEI(CCEIProcessingParams cceiProcessingParams)
    {
        await _coProcessor.Process(cceiProcessingParams.CO);
    }

    private async Task GenerateTagListFile(ITagListGenerator tagListGenerator, TagListsGenerationParams args)
    {
        IEnumerable<Common.Core.Domain.VehicleTransponderModel> transponders = null;
        if (args.ListType == ListTypes.White)
        {
            transponders = await _serviceInventoryFacade.GetWhiteListTransponders();
        }
        else
        {
            //black and grey lists still need to pull transponders as we need to workout all the available COs
            transponders = await _serviceInventoryFacade.GetBlackListTransponders();
        }

        _logger.LogInformation($"Loaded {transponders.Count()} {args.ListType}-list tags after validations");

        if (transponders.Any() || args.ListType == ListTypes.Grey)
        {
            await tagListGenerator.GenerateXMLFiles(transponders, args.ListType);
        }
        else
        {
            _logger.LogInformation($"No further processing required.");
        }
    }
}