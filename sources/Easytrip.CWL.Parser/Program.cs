﻿using Easytrip.CWL.Parser.Config;
using Easytrip.CWL.Parser.XML;
using Microsoft.Extensions.Configuration;
using System.Xml;

namespace Easytrip.CWL.Parser
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false)
                .Build();

            var easytripCOs = config
                .GetSection("EasytripCOs")
                .Get<IEnumerable<Config.COConfig>>();
            var easytripCMs = config
                .GetSection("EasytripCMs")
                .Get<string[]>();

            using var file = File.OpenRead(args[0]);
            var doc = new XmlDocument();
            doc.Load(file);

            var listNode = doc?.LastChild?.FirstChild;
            if (listNode == null) Console.WriteLine("Empty XML");

            var nonEtCoNodes = GetNonETCoNodes(listNode, easytripCOs);
            var coItems = ParseCOItems(nonEtCoNodes)
                .DistinctBy(d => d.TagNoHex + d.ContextMark);

            GenerateSqlInsert(coItems);

            Console.WriteLine("Hello, World!");
        }

        private static void GenerateSqlInsert(IEnumerable<Item> coItems)
        {
            using var stream = File.Open("cwl.sql", FileMode.Create, FileAccess.Write);
            using var sw = new StreamWriter(stream);
            var createTableSql = @"
create table #cwl (
id int not null identity, 
tagNo varchar(15) not null,
contextMark varchar(12) not null,
reg varchar(15) null
);

";
            sw.Write(createTableSql);

            var insertSql = @"
insert into #cwl (tagNo, contextMark, reg)
values
";
            sw.Write(insertSql);

            int counter = 0;
            foreach (var coItem in coItems)
            {
                var regSql = coItem.Reg == null ? "null" : $"'{coItem.Reg}'";
                var insertRowSql = $"('{coItem.TagNoHex}', '{coItem.ContextMark}', {regSql})";
                if (counter >= 500)
                {
                    insertRowSql += ";\r\n";
                    insertRowSql += insertSql;
                    counter = 0;
                }
                else
                {
                    insertRowSql += ",";
                    counter++;
                }

                sw.WriteLine(insertRowSql);
            }
        }

        private static IEnumerable<XML.Item> ParseCOItems(IEnumerable<XmlElement> nonEtCoNodes)
        {
            foreach (XmlElement coNode in nonEtCoNodes)
            {
                foreach (XmlElement item in coNode.ChildNodes)
                {
                    yield return new XML.Item
                    {
                        ContextMark = item.Attributes["cm"]?.Value,
                        TagNoHex = item.Attributes["obu"]?.Value,
                        Reg = item.Attributes["vrn"]?.Value
                    };
                }
            }
        }

        private static IEnumerable<XmlElement> GetNonETCoNodes(XmlNode listNode, IEnumerable<COConfig> easytripCOs)
        {
            foreach (XmlElement coNode in listNode.ChildNodes)
            {
                var co = coNode.Attributes["id"];
                if (easytripCOs.Any(a => a.CO.ToString() == co.Value) == false)
                {
                    yield return coNode;
                }
            }
        }
    }
}