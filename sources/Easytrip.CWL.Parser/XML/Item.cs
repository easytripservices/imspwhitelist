﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.CWL.Parser.XML
{
    internal class Item
    {
        public string ContextMark { get; set; }
        public string TagNoHex { get; set; }
        public string Reg { get; set; }
    }
}
