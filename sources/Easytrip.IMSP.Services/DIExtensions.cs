﻿using Microsoft.Extensions.DependencyInjection;

namespace Easytrip.IMSP.Services
{
    public static class DIExtensions
    {
        public static IServiceCollection AddIMSPServices(this IServiceCollection services)
        {
            services.AddTransient<IFileValidator, FileValidator>();
            services.AddTransient<ICOProcessor, COProcessor>();
            services.AddTransient<ITagListGenerator, NewTagListGenerator>();
            return services;
        }
    }
}