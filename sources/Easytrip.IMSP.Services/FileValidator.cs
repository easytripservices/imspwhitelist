﻿using Easytrip.IMSP.Services.Models;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Xml.Serialization;

namespace Easytrip.IMSP.Services
{
    public interface IFileValidator
    {
        Stream Unzip(Stream stream);
        Stream GenerateACKFile(AckResultCodes code, string message);
        ZipInfo Zip(Stream stream, string fileNameBase = null);
        string GenerateCCEIFileName(string fileNameBase = null);
    }

    public class FileValidator : IFileValidator
    {
        private ILogger<FileValidator> _logger;

        public FileValidator(ILogger<FileValidator> logger)
        {
            _logger = logger;
        }

        public Stream Unzip(Stream stream)
        {
            using var zip = new ZipArchive(stream, ZipArchiveMode.Read, true);
            var ccei = zip.Entries.FirstOrDefault(a => a.Name.EndsWith("-CCEI.xml"));
            if (ccei == null) throw new FileNotFoundException("CCEI file not found in archive");

            var checksum = zip.Entries.FirstOrDefault(a => a.Name.EndsWith("-CCEI.checksum.xml"));
            if (checksum == null) throw new FileNotFoundException("Checksum file not found in archive");

            using var cceiArchStream = ccei.Open();
            using var checksumStream = checksum.Open();
            var msCCEIArchStream = new MemoryStream();
            cceiArchStream.CopyTo(msCCEIArchStream);
            msCCEIArchStream.Position = 0;

            bool result = false;
            try
            {
                result = ValidateChecksum(msCCEIArchStream, checksumStream);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Problem validating checksum: {ccei.Name}");
            }

            if (result == false)
            {
                msCCEIArchStream.Dispose();
                throw new ChecksumFailedException(ccei.Name);
            }

            msCCEIArchStream.Position = 0;
            return msCCEIArchStream;
        }

        public string GenerateCCEIFileName(string fileNameBase = null)
        {
            var timespamp = fileNameBase ?? DateTime.UtcNow.ToString("yyyyMMddHHmmss");
            var nameBase = $"{timespamp}-CCEI";
            return nameBase;
        }

        public ZipInfo Zip(Stream stream, string fileNameBase = null)
        {
            var nameBase = GenerateCCEIFileName(fileNameBase);
            var cceiName = $"{nameBase}.xml";
            var ms = new MemoryStream();
            using (var zip = new ZipArchive(ms, ZipArchiveMode.Create, true))
            {
                var cceiArchEntry = zip.CreateEntry(cceiName);
                using (var cceiArchStream = cceiArchEntry.Open())
                {
                    stream.Position = 0; //just in case
                    stream.CopyTo(cceiArchStream);
                }

                var checksumArchEntry = zip.CreateEntry($"{nameBase}.checksum.xml");
                using (var checksumArchStream = checksumArchEntry.Open())
                {
                    stream.Position = 0;
                    using var checksumStream = GenerateChecksumFile(stream, cceiName);
                    checksumStream.CopyTo(checksumArchStream);
                }
            }

            ms.Position = 0;
            return new ZipInfo(ms, $"{nameBase}.zip");
        }

        internal bool ValidateChecksum(Stream cceiStream, Stream checksumStream)
        {
            var xs = new XmlSerializer(typeof(CCEIChecksum));
            var checksum = xs.Deserialize(checksumStream) as CCEIChecksum;
            if (checksum == null) return false;

            var computedChecksum = ComputeHash(cceiStream);

            return string.Equals(checksum.Checksum, computedChecksum, StringComparison.OrdinalIgnoreCase);
        }

        internal Stream GenerateChecksumFile(Stream stream, string cceiName)
        {
            var hash = ComputeHash(stream);
            var checksum = new CCEIChecksum
            {
                Checksum = hash,
                Filename = cceiName
            };

            var ms = new MemoryStream();
            var xmlSerializer = new XmlSerializer(typeof(CCEIChecksum));
            xmlSerializer.Serialize(ms, checksum);
            ms.Position = 0;

            return ms;
        }

        internal string ComputeHash(Stream stream)
        {
            using (var hash = System.Security.Cryptography.SHA512.Create())
            {
                var hashedInputBytes = hash
                    .ComputeHash(stream)
                    .Select(s => s
                        .ToString("X2")
                        .ToLower());
                var computedChecksum = string.Join(string.Empty, hashedInputBytes);
                return computedChecksum;
            }
        }

        public Stream GenerateACKFile(AckResultCodes code, string message)
        {
            var ack = new CCEI_ACK
            {
                ErrorCode = (int)code,
                ErrorLabel = message,
                Free = message,
                ReceivedDate = DateTime.UtcNow
            };

            var ms = new MemoryStream();
            var xmlSerializer = new XmlSerializer(ack.GetType());
            xmlSerializer.Serialize(ms, ack);
            ms.Flush();
            ms.Position = 0;

            return ms;
        }
    }
}