﻿using Amazon.S3;
using Amazon.S3.Model;
using Easytrip.IMSP.Common.Core.Config;
using Easytrip.IMSP.Common.Core.Domain;
using Easytrip.IMSP.Common.Core.Utils;
using Easytrip.IMSP.Facades;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Services
{
    public class NewTagListGenerator : ITagListGenerator
    {
        private ILogger<NewTagListGenerator> _logger;
        private IIMSPFacade _imspFacade;
        private IAmazonS3 _amazonS3;
        private IXMLHelper _xmlHelper;
        private IMSPConfig _config;
        private string _imspBucketName;
        private string _blXsdObjectPath;
        private string _wlXsdObjectPath;

        public NewTagListGenerator(
            ILogger<NewTagListGenerator> logger,
            IMSPConfig config,
            IIMSPFacade imspFacade,
            IAmazonS3 amazonS3,
            IXMLHelper xmlHelper)
        {
            _logger = logger;
            _imspFacade = imspFacade;
            _amazonS3 = amazonS3;
            _xmlHelper = xmlHelper;

            _config = config;
            _imspBucketName = config.BucketName;
            _blXsdObjectPath = config.XSD.BLPath;
            _wlXsdObjectPath = config.XSD.WLPath;
        }

        public async Task GenerateXMLFiles(IEnumerable<VehicleTransponderModel> items, ListTypes listType)
        {
            var fileTimestamp = DateTime.UtcNow.ToString("yyyyMMddHHmmss");

            try
            {
                var coLists = items
                    .Select(o => o.Id)
                    .Distinct()
                    .ToList();

                _logger.LogInformation($"Total of {coLists.Count} CO's found");

                foreach (var coItem in coLists)
                {
                    string postfix = "";
                    if (listType == ListTypes.Black) postfix = "BL";
                    else if (listType == ListTypes.White) postfix = "WL";
                    else if (listType == ListTypes.Grey)
                    {
                        postfix = "GL";
                        items = Enumerable.Empty<VehicleTransponderModel>(); //this is because grey list is always empty
                    }
                    else throw new ArgumentOutOfRangeException($"Invalid list type: '{listType}'");

                    _logger.LogInformation($"Starting to write XML Contents for CO {coItem}");
                    using (Stream memStream = _xmlHelper.WriteXmlContents(coItem, items, fileTimestamp, listType))
                    {
                        _logger.LogInformation($"Finished writing XML Contents for CO {coItem}");


                        _logger.LogInformation($"Starting XML validation");
                        var xsdPath = listType == ListTypes.White ? _wlXsdObjectPath : _blXsdObjectPath;
                        if (await _xmlHelper.PerformXsdValidation(_amazonS3, memStream, _imspBucketName, xsdPath))
                        {
                            _logger.LogInformation($"XML validated successfully");

                            memStream.Position = 0;
                            //using var zipStream = _fileValidator.Zip(memStream, _fileEmissionDatetimeString);
                            var fileName = $"{fileTimestamp}-{postfix}.xml";

                            var imspSftpDirectory = _config.SFTP.OutgoingPath ?? string.Empty;
                            var sftpPath = SFTPPath.Join(imspSftpDirectory, fileName);
                            _imspFacade.SendFile(coItem, memStream, sftpPath);

                            
                            var s3FilePath= SFTPPath.Join(coItem.ToString(), _config.ObjectPrefix, fileName);
                            var s3LatestFilePath = SFTPPath.Join(coItem.ToString(), _config.ObjectPrefix, $"_latest{listType}List.xml");
                            await WriteToS3( memStream, s3FilePath);
                            await CopyFileAsLatestInS3( s3FilePath, s3LatestFilePath);
                            
                        }
                        else
                        {
                            _logger.LogError($"Problem validating XML against XSD '{xsdPath}'");
                        }
                    }
                }

                _logger.LogInformation($"SUCCESSFUL - XML generation finished");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Problem occurred during XML generation");
                throw;
            }
        }

        private async Task WriteToS3(Stream memStream, string keyFilePath)
        {
            try
            {
                var res = await _amazonS3
                                 .PutObjectAsync(new PutObjectRequest
                                 {
                                     BucketName = _config.BucketName,
                                     Key = keyFilePath,
                                     ContentType = "text/xml",
                                     InputStream = memStream
                                 });

                if (res.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    _logger.LogInformation($"Finished uploading file: " + keyFilePath);
                }
                else throw new Exception($"Failed to upload file to S3. Filename: {keyFilePath}, Status Code: {res.HttpStatusCode}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Method:WriteToS3, BucketName:{_config?.BucketName}, KeyFilePath:{keyFilePath}. Problem writing to S3 bucket.");
            }
        }

        private async Task CopyFileAsLatestInS3(string sourceKey, string destinationKey)
        {
            try
            {
                var copyRequest = new CopyObjectRequest
                {
                    SourceBucket = _config.BucketName,
                    SourceKey = sourceKey,
                    DestinationBucket = _config.BucketName,
                    DestinationKey = destinationKey
                };

                var response = await _amazonS3.CopyObjectAsync(copyRequest);

                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    _logger.LogInformation($"Successfully copied file as latest: {destinationKey}");
                }
                else
                {
                    throw new Exception($"Failed to copy file as latest in S3. Source: {sourceKey}, Destination: {destinationKey}, Status Code: {response.HttpStatusCode}");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Method:CopyFileAsLatestInS3, BucketName:{_config?.BucketName}, SourceKey:{sourceKey}, DestinationKey:{destinationKey}. Problem copying file in S3 bucket.");
            }
        }

    }
}