﻿using Easytrip.IMSP.Common.Core.Domain;
using Easytrip.IMSP.Common.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Services
{
    public interface ITagListGenerator
    {
        Task GenerateXMLFiles(IEnumerable<VehicleTransponderModel> items, ListTypes listType);
    }
}
