﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Services
{
    public class ChecksumFailedException : Exception
    {
        public ChecksumFailedException(string cceiFile) : base($"CCEI: {cceiFile}")
        {
        }
    }
}