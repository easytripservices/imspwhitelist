﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Services.Models
{
    public class ZipInfo : IDisposable
    {
        public Stream Stream { get; private set; }
        public string FileName { get; private set; }

        public ZipInfo(Stream stream, string fileName)
        {
            this.Stream = stream;
            this.FileName = fileName;
        }

        public void Dispose()
        {
            Stream.Dispose();
        }
    }
}