﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Easytrip.IMSP.Services.Models
{
    [XmlRoot("data")]
    public class CCEI_ACK
    {
        [XmlElement("received_date")]
        public string ReceivedDateStr
        {
            get { return ReceivedDate.ToString("yyyy-MM-dd HH:mm:ss"); }
            set { ReceivedDate = DateTime.Parse(value); }
        }

        [XmlIgnore]
        public DateTime ReceivedDate { get; set; }

        [XmlElement("error_code")]
        public int ErrorCode { get; set; }

        [XmlElement("error_label")]
        public string ErrorLabel { get; set; }

        [XmlElement("free")]
        public string Free { get; set; }
    }
}