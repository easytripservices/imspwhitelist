﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Easytrip.IMSP.Services.Models
{
    [XmlRoot("data")]
    public class CCEIChecksum
    {
        [XmlElement("filename")]
        public string Filename { get; set; }

        [XmlElement("checksum")]
        public string Checksum { get; set; }
    }
}