﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Services.Models
{
    public enum AckResultCodes : int
    {
        Success = 0,
        ChecksumVerificationFailed = -715,
        GenericError = -719
    }
}