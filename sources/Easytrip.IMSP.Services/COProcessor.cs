﻿using Easytrip.IMSP.Common.Core.Config;
using Easytrip.IMSP.Facades;
using Easytrip.IMSP.Services.Models;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Easytrip.IMSP.Services
{
    public interface ICOProcessor
    {
        Task Process(int co);
    }

    public class COProcessor : ICOProcessor
    {
        private ILogger<COProcessor> _logger;
        private IMSPConfig _config;
        private IFileValidator _fileValidator;
        private IIMSPFacade _imspFacade;
        private IBillingFacade _billingFacade;

        public COProcessor(
            ILogger<COProcessor> logger,
            IMSPConfig config,
            IFileValidator fileValidator,
            IIMSPFacade imspFacade,
            IBillingFacade billingFacade)
        {
            _logger = logger;
            _config = config;
            _fileValidator = fileValidator;
            _imspFacade = imspFacade;
            _billingFacade = billingFacade;
        }

        public async Task Process(int co)
        {
            await ProcessIncomingFiles(co);
        }

        private async Task ProcessIncomingFiles(int co)
        {
            var incomingDir = _config.SFTP.IncomingPath;

            var files = _imspFacade.ListIncomingFiles(co, incomingDir);

            _logger.LogInformation($"Incoming files to process: {files.Count()}");

            foreach (var file in files)
            {
                var path = SFTPPath.Join(incomingDir, file);

                if (file.EndsWith("-CCEI-ack.xml", StringComparison.OrdinalIgnoreCase))
                {
                    ProcessIncomingAck(co, path);
                }
                else if (file.EndsWith("-CCEI.zip", StringComparison.OrdinalIgnoreCase))
                {
                    await ProcessIncomingCCEIZip(co, path);
                }
                else if (file.EndsWith("-CCEI.xml", StringComparison.OrdinalIgnoreCase))
                {
                    await ProcessIncomingCCEIXml(co, path);
                }
                else
                {
                    _logger.LogInformation($"Skipping file '{file}'");
                }
            }
        }

        internal async Task ProcessIncomingCCEIXml(int co, string path)
        {
            _logger.LogInformation($"Processing '{path}'");

            var file = Path.GetFileName(path);
            using var fileStream = _imspFacade.GetFile(co, path);
            await _billingFacade.UploadCCEI(fileStream, co, file);
            _imspFacade.DeleteFile(co, path);
        }

        internal async Task ProcessIncomingCCEIZip(int co, string path)
        {
            _logger.LogInformation($"Processing '{path}'");

            var file = Path.GetFileName(path);

            try
            {                
                using var fileStream = _imspFacade.GetFile(co, path);
                using var cceiStream = _fileValidator.Unzip(fileStream);

                //if the file unzipped, then the checksum passed
                UploadSuccessfulAck(co, path);

                await _billingFacade.UploadCCEI(cceiStream, co, file);

                _imspFacade.DeleteFile(co, path);
            }
            catch (ChecksumFailedException ex)
            {
                _logger.LogError($"Problem processing incoming file: '{file}'", ex);
                ProcessChecksumFailed(co, file);
            }
        }

        private void ProcessIncomingAck(int co, string path)
        {
            using var ackStream = _imspFacade.GetFile(co, path);
            using var sr = new StreamReader(ackStream);

            var str = sr.ReadToEnd();
            _logger.LogInformation($"ACK Received: {str}");

            ackStream.Position = 0;
            var xs = new XmlSerializer(typeof(CCEI_ACK));
            var ack = xs.Deserialize(ackStream) as CCEI_ACK;

            if (ack.ErrorCode != (int)AckResultCodes.Success)
            {
                _logger.LogError($"IMSP ACK error received: '{path}', '{ack.ErrorLabel}', '{ack.ErrorCode}', '{ack.Free}'");
            }

            _imspFacade.DeleteFile(co, path);
        }

        private void UploadSuccessfulAck(int co, string path)
        {
            var fileName = Path.GetFileNameWithoutExtension(path); //the file name should be something like '202200222113712-CCEI.zip'

            using var ackStream = _fileValidator.GenerateACKFile(Models.AckResultCodes.Success, fileName);
            //file name should be something like 202200222113712-CCEI-ack.xml
            var path2 = SFTPPath.Join(_config.SFTP.OutgoingPath, $"{fileName}-ack.xml");
            _imspFacade.SendFile(co, ackStream, path2);
        }

        private void ProcessChecksumFailed(int co, string file)
        {
            _logger.LogError($"IMSP checksum failed: CO: '{co}', File: '{file}'");

            using var ackStream = _fileValidator.GenerateACKFile(Models.AckResultCodes.ChecksumVerificationFailed, file);
            _imspFacade.SendFile(co, ackStream, _config.SFTP.OutgoingPath);
        }
    }
}