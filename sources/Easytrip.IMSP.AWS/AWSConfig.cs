﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easytrip.IMSP.AWS
{
    public class AWSConfig
    {
        public string AccessKey { get; set; }
        public string Secret { get; set; }
    }
}
