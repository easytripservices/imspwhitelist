﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.SecretsManager;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Easytrip.IMSP.AWS
{
    public static class DIExtensions
    {
        public static IServiceCollection AddAWSClients(this IServiceCollection services)
        {
            services.AddSingleton<AWSConfig>(sp =>
            {
                var config = sp.GetRequiredService<IConfiguration>();
                var awsConfig = config.GetSection("AWS").Get<AWSConfig>();
                return awsConfig;
            });

            services.AddTransient<IAmazonS3>(sp =>
            {
                var config = sp.GetService<AWSConfig>();
                AmazonS3Client client = null;
                if (string.IsNullOrEmpty(config?.AccessKey) || string.IsNullOrEmpty(config?.Secret))
                {
                    client = new AmazonS3Client(RegionEndpoint.EUWest1);
                }
                else
                {
                    client = new AmazonS3Client(new BasicAWSCredentials(config.AccessKey, config.Secret), RegionEndpoint.EUWest1);
                }

                return client;
            });

            services.AddTransient<IAmazonSecretsManager>(sp =>
            {
                var config = sp.GetService<AWSConfig>();
                AmazonSecretsManagerClient client = null;
                if (string.IsNullOrEmpty(config?.AccessKey) || string.IsNullOrEmpty(config?.Secret))
                {
                    client = new AmazonSecretsManagerClient(RegionEndpoint.EUWest1);
                }
                else
                {
                    client = new AmazonSecretsManagerClient(new BasicAWSCredentials(config.AccessKey, config.Secret), RegionEndpoint.EUWest1);
                }

                return client;
            });

            return services;
        }
    }
}