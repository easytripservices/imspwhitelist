using Easytrip.IMSP.Common.Core.Domain;
using Easytrip.IMSP.Common.Core.Utils;
using Easytrip.IMSP.Facades;
using Easytrip.IMSP.ListTransfer.Lambda.Models;
using Easytrip.IMSP.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Easytrip.IMSP.ListTransfer.Lambda.Tests;

[TestClass]
public class FunctionTest
{
    private Mock<IServiceInventoryFacade> _siMock;
    private Mock<ITagListGenerator> _newTagGeneratorMock;
    private Mock<ICOProcessor> _coProcessorMock;
    
    [TestInitialize]
    public void Init()
    {
        var services = new ServiceCollection();
        services.AddLogging();

        _siMock = new Mock<IServiceInventoryFacade>();
        services.AddSingleton(_siMock.Object);

        _newTagGeneratorMock = new Mock<ITagListGenerator>();
        services.AddSingleton(_newTagGeneratorMock.Object);

        _coProcessorMock = new Mock<ICOProcessor>();
        services.AddSingleton(_coProcessorMock.Object);

        IoC.Container= services.BuildServiceProvider();
    }

    [TestMethod]
    public async Task TestEmptyWhiteListIsNotExported_Obsolete()
    {
        var payload = new Models.ExecutionInfo
        {
            Action = Models.ActionTypes.GenerateTagLists_New,
            ActionParams = System.Text.Json.JsonSerializer.SerializeToElement(new TagListsGenerationParams
            {
                ListType = Common.Core.Utils.ListTypes.White
            })
        };

        var func = new Function();
        await func.FunctionHandler(payload, null);

        _siMock.Verify(v => v.GetWhiteListTransponders());
        _newTagGeneratorMock
            .Verify(v => v.GenerateXMLFiles(It.IsAny<IEnumerable<VehicleTransponderModel>>(), It.IsAny<ListTypes>()), Times.Never());
    }

    [TestMethod]
    public async Task TestEmptyBlackListIsNotExported_Obsolete()
    {
        var payload = new Models.ExecutionInfo
        {
            Action = Models.ActionTypes.GenerateTagLists_New,
            ActionParams = System.Text.Json.JsonSerializer.SerializeToElement(new TagListsGenerationParams
            {
                ListType = Common.Core.Utils.ListTypes.Black
            })
        };

        var func = new Function();
        await func.FunctionHandler(payload, null);

        _siMock.Verify(v => v.GetBlackListTransponders());
        _newTagGeneratorMock
            .Verify(v => v.GenerateXMLFiles(It.IsAny<IEnumerable<VehicleTransponderModel>>(), It.IsAny<ListTypes>()), Times.Never());
    }

    [TestMethod]
    public async Task TestMoveCCEI()
    {
        var payload = new Models.ExecutionInfo
        {
            Action = Models.ActionTypes.MoveCCEI,
            ActionParams = System.Text.Json.JsonSerializer.SerializeToElement(new CCEIProcessingParams
            {
                CO = 12
            })
        };

        var func = new Function();
        await func.FunctionHandler(payload, null);

        _coProcessorMock.Verify(v => v.Process(12));
    }

    [TestMethod]
    public async Task TestEmptyWhiteListIsNotExported_New()
    {
        var payload = new Models.ExecutionInfo
        {
            Action = Models.ActionTypes.GenerateTagLists_New,
            ActionParams = System.Text.Json.JsonSerializer.SerializeToElement(new TagListsGenerationParams
            {
                ListType = Common.Core.Utils.ListTypes.White
            })
        };

        var func = new Function();
        await func.FunctionHandler(payload, null);

        _siMock.Verify(v => v.GetWhiteListTransponders());
        _newTagGeneratorMock
            .Verify(v => v.GenerateXMLFiles(It.IsAny<IEnumerable<VehicleTransponderModel>>(), It.IsAny<ListTypes>()), Times.Never());
    }

    [TestMethod]
    public async Task TestEmptyBlackListIsNotExported_New()
    {
        var payload = new Models.ExecutionInfo
        {
            Action = Models.ActionTypes.GenerateTagLists_New,
            ActionParams = System.Text.Json.JsonSerializer.SerializeToElement(new TagListsGenerationParams
            {
                ListType = Common.Core.Utils.ListTypes.Black
            })
        };

        var func = new Function();
        await func.FunctionHandler(payload, null);

        _siMock.Verify(v => v.GetBlackListTransponders());
        _newTagGeneratorMock
            .Verify(v => v.GenerateXMLFiles(It.IsAny<IEnumerable<VehicleTransponderModel>>(), It.IsAny<ListTypes>()), Times.Never());
    }
}