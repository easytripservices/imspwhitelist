﻿using Microsoft.Extensions.DependencyInjection;

namespace Easytrip.IMSP.Facades
{
    public static class DIExtensions
    {
        public static IServiceCollection AddIMSPFacades(this IServiceCollection services)
        {
            services.AddTransient<IIMSPFacade, IMSPFacade>();
            services.AddTransient<IBillingFacade, BillingFacade>();
            services.AddTransient<IServiceInventoryFacade, ServiceInventoryFacade>();
            services.AddHttpClient();
            return services;
        }
    }
}