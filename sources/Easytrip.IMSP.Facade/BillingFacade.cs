﻿using Amazon.S3;
using Amazon.S3.Model;
using Easytrip.IMSP.Common.Core.Config;
using Easytrip.IMSP.Common.Core.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Facades
{
    public interface IBillingFacade
    {
        Task UploadCCEI(Stream stream, int co, string fileName);
    }

    public class BillingFacade : IBillingFacade
    {
        private ILogger<BillingFacade> _logger;
        private IAmazonS3 _amazonS3;
        private IXMLHelper _xmlHelper;
        private string _billingSuccessBucket;
        private string _billingErrorBucket;
        private string _billingXsdBucket;
        private string _billingXsdPath;

        public BillingFacade(
            ILogger<BillingFacade> logger,
            IAmazonS3 amazonS3,
            BillingConfig config,
            IXMLHelper xmlHelper)
        {
            _logger = logger;
            _amazonS3 = amazonS3;
            _xmlHelper = xmlHelper;

            _billingSuccessBucket = config.SuccessBucket; ;
            _billingErrorBucket = config.ErrorBucket;
            _billingXsdBucket = config.XSD.Bucket;
            _billingXsdPath = config.XSD.FilePath;
        }

        public async Task UploadCCEI(Stream stream, int co, string fileName)
        {
            _logger.LogInformation($"Uploading file '{fileName}' for CO {co}");

            if (await ValidateFile(stream))
            {
                await UploadToSuccessBucket(stream, co, fileName);
            }
            else
            {
                await UploadToErrorBucket(stream, co, fileName);
            }
        }

        private async Task UploadToSuccessBucket(Stream stream, int co, string fileName)
        {
            await UploadToBucket(stream, _billingSuccessBucket, ConstructObjectKey(co, fileName));
        }

        private async Task UploadToErrorBucket(Stream stream, int co, string fileName)
        {
            await UploadToBucket(stream, _billingErrorBucket, ConstructObjectKey(co, fileName));
        }

        private async Task UploadToBucket(Stream stream, string bucketName, string fileName)
        {
            var req = new PutObjectRequest
            {
                BucketName = bucketName,
                InputStream = stream,
                Key = fileName
            };
            var resp = await _amazonS3.PutObjectAsync(req);

            if (resp.HttpStatusCode != System.Net.HttpStatusCode.OK)
            {
                _logger.LogError($"Unexpected response for bucket {bucketName}: {resp.HttpStatusCode}");
            }
            else
            {
                _logger.LogInformation($"File '{req.Key}' uploaded successfully to bucket {bucketName}");
            }
        }

        internal async Task<bool> ValidateFile(Stream stream)
        {
            if (string.IsNullOrWhiteSpace(_billingXsdBucket) || string.IsNullOrWhiteSpace(_billingXsdPath))
            {
                _logger.LogInformation("XSD validation not configured, skipping...");
                return true;
            }

            var pos = stream.Position;

            var result = await _xmlHelper.PerformXsdValidation(_amazonS3, stream, _billingXsdBucket, _billingXsdPath);

            stream.Position = pos;

            return result;
        }

        private string ConstructObjectKey(int co, string fileName)
        {
            //the object key should be in the following format: IEA.1.yyyyMMddHHmmss-CCEI.xml or IEA.51.yyyyMMddHHmmss-CCEI.xml
            //the incoming files are in the following format: 20200901123030-CCEI.xml
            return $"IEA.{co}.{fileName}";
        }
    }
}