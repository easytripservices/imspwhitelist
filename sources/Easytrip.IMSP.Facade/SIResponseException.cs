﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Facades
{
    public class SIResponseException : Exception
    {
        public SIResponseException(string message) : base(message)
        {
        }
    }
}