﻿using Easytrip.Global.Request;
using Easytrip.Global.Response;
using Easytrip.IMSP.Common.Core.Builders;
using Easytrip.IMSP.Common.Core.Domain;
using Easytrip.IMSP.Common.Core.Validations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Facades
{
    public interface IServiceInventoryFacade
    {
        Task<IEnumerable<VehicleTransponderModel>> GetBlackListTransponders();
        Task<IEnumerable<VehicleTransponderModel>> GetWhiteListTransponders();
    }

    public class ServiceInventoryFacade : IServiceInventoryFacade
    {
        private ILogger<ServiceInventoryFacade> _logger;
        private HttpClient _httpClient;

        public ServiceInventoryFacade(
            ILogger<ServiceInventoryFacade> logger,
            HttpClient httpClient,
            IConfiguration config)
        {
            _logger = logger;
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri(config.GetValue<string>("ServiceInventory:BaseUrl"));
        }

        public Task<IEnumerable<VehicleTransponderModel>> GetBlackListTransponders()
        {
            return GetTransponders("api/WhiteList/GetBlackList");
        }

        public Task<IEnumerable<VehicleTransponderModel>> GetWhiteListTransponders()
        {
            return GetTransponders("api/WhiteList/GetWhiteList");
        }

        private async Task<IEnumerable<VehicleTransponderModel>> GetTransponders(string path)
        {
            //#LOAD DATA
            var request = new VehicleTransponderRequestModel().ToDataRequest();

            _logger.LogInformation($"Retrieving transponders from Service Inventory");

            var response = await _httpClient.PostAsJsonAsync(path, request);
            if (response.IsSuccessStatusCode == false)
            {
                throw new SIResponseException($"Unexpected response received: {response.StatusCode}");
            }

            //var contentStr=await response.Content.ReadAsStringAsync();

            var content = await response.Content.ReadAsAsync<SomeMaybe<DataResponse<List<VehicleTransponderResponseModel>>>>();
            _logger.LogInformation($"ServiceInventory returned {content?.Data?.ResponseData?.Count} transponders");

            var list = content.Data.ResponseData
                .Where(w => TransponderValidation.IsVehicleTransponderValid(w))
                .Select(s => VehicleTransponderBuilder.Mapper(s));

            return list;
        }
    }
}