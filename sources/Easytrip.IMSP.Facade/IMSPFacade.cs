﻿using Amazon.SecretsManager;
using Easytrip.IMSP.Common.Core.Config;
using Microsoft.Extensions.Logging;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Easytrip.IMSP.Facades
{
    public interface IIMSPFacade
    {
        IEnumerable<string> ListIncomingFiles(int co, string path);
        Stream GetFile(int co, string path);
        void SendFile(int co, Stream stream, string path);
        void DeleteFile(int co, string path);
    }

    public class IMSPFacade : IIMSPFacade, IDisposable
    {
        private ILogger<IMSPFacade> _logger;
        private IMSPConfig _config;
        private IAmazonSecretsManager _secretsManager;
        private Dictionary<int, SftpClient> _sftpClients = new Dictionary<int, SftpClient>();

        public IMSPFacade(
            ILogger<IMSPFacade> logger,
            IMSPConfig config,
            IAmazonSecretsManager secretsManager)
        {
            _logger = logger;
            _config = config;
            _secretsManager = secretsManager;
        }

        public IEnumerable<string> ListIncomingFiles(int co, string path)
        {
            try
            {
                var client = GetClient(co);
                var result = client
                    .ListDirectory(path)
                    .Where(w => w.IsRegularFile)
                    .Select(s => s.Name);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("Problem listing SFTP files", ex);
                throw;
            }
        }

        private SftpClient GetClient(int co)
        {
            if (_config.SFTP.COPortMapping.TryGetValue(co.ToString("D2"), out var coPort) == false) throw new Exception($"No mapping found for CO '{co}'");

            if (_sftpClients.TryGetValue(co, out var client) && CheckClientConnectivity(client))
            {
                return client;
            }
            else
            {
                if (client != null)
                {
                    try { client.Dispose(); }
                    catch { }
                }

                client = CreateClient(coPort);
                _sftpClients[co] = client;

                if (CheckClientConnectivity(client) == false)
                {
                    throw new Exception("Cannot connect to SFTP server");
                }
            }

            return client;
        }

        private bool CheckClientConnectivity(SftpClient client)
        {
            try
            {
                if (client.IsConnected == false)
                {
                    _logger.LogInformation($"Attempting to connect: {client.ConnectionInfo.Host}:{client.ConnectionInfo.Port}");
                    client.Connect();

                    return client.IsConnected;
                }
                else
                {
                    _logger.LogInformation("Checking SFTP client on port {coPort}", client.ConnectionInfo.Port);

                    //try executing a command against a connection in case it was aborted by the server but the client still "thinks" it's connected
                    client.ListDirectory("/");//if it doesn't throw, then the connection is still alive and can be used later
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Problem connecting to SFTP server");
                return false;
            }
        }

        private SftpClient CreateClient(int coPort)
        {
            _logger.LogInformation("Creating new SFTP client on port {coPort}", coPort);

            SftpClient client = null;

            if (string.IsNullOrWhiteSpace(_config.SFTP.Password) == false)
            {
                client = new SftpClient(_config.SFTP.Host, coPort, _config.SFTP.Username, _config.SFTP.Password);
            }
            else
            {
                using var privateKey = GetPrivateKey();

                client = new SftpClient(_config.SFTP.Host, coPort, _config.SFTP.Username, privateKey);
            }

            return client;
        }

        private PrivateKeyFile GetPrivateKey()
        {
            _logger.LogDebug("Retrieving SSH private key");

            var task = _secretsManager
                 .GetSecretValueAsync(new Amazon.SecretsManager.Model.GetSecretValueRequest
                 {
                     SecretId = _config.SFTP.SecretName
                 });
            task.Wait();

            var key = task.Result.SecretString;
            using (var ms = new MemoryStream())
            using (var sw = new StreamWriter(ms))
            {
                sw.Write(key);
                sw.Flush();

                ms.Position = 0;
                return new PrivateKeyFile(ms);
            }
        }

        public Stream GetFile(int co, string path)
        {
            var client = GetClient(co);
            return client.OpenRead(path);
        }

        public void Dispose()
        {
            foreach (var val in _sftpClients.Values)
            {
                try
                {
                    val?.Dispose();
                }
                catch
                {
                }
            }
        }

        public void SendFile(int co, Stream stream, string path)
        {
            _logger.LogInformation($"Uploading file '{path}'");

            var client = GetClient(co);
            using (var writeStream = client.OpenWrite(path))
            {
                stream.CopyTo(writeStream);
            }
        }

        public void DeleteFile(int co, string path)
        {
            _logger.LogInformation($"Deleting file '{path}'");

            var client = GetClient(co);
            client.DeleteFile(path);
        }
    }
}