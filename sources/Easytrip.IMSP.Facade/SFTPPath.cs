﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Facades
{
    public static class SFTPPath
    {
        private const string CO_PATH_PLACEHOLDER = "{{co}}";

        public static string Join(params string[] paths)
        {
            var path = string.Join("/", paths);
            path = Regex.Replace(path, @"\/+", "/");
            return path;
        }
    }
}