﻿using Easytrip.IMSP.Common.Core;
using Easytrip.IMSP.Facades;
using MELT;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Easytrip.IMSP.Services.Tests
{
    [TestClass]
    public class COProcessorTest
    {
        private Mock<IIMSPFacade> _imspFacadeMock;
        private Mock<IBillingFacade> _billingFacadeMock;
        private ServiceProvider _provider;
        private ITestLoggerSink _testSink;
        private ServiceCollection _services;

        [TestInitialize]
        public void Init()
        {
            _services = new ServiceCollection();

            _services.AddLogging(l => l.AddTest());

            _services.AddTransient<ICOProcessor, COProcessor>();

            var config = new ConfigurationBuilder()
                .AddInMemoryCollection()
                .Build();
            config["IMSP:SFTP:IncomingPath"] = "in";
            config["IMSP:SFTP:OutgoingPath"] = "out";
            _services.AddSingleton<IConfiguration>(config);

            _services.AddCommonUtils();

            _services.AddTransient<IFileValidator, FileValidator>();

            _imspFacadeMock = new Mock<IIMSPFacade>();
            _services.AddSingleton(_imspFacadeMock.Object);

            _billingFacadeMock = new Mock<IBillingFacade>();
            _services.AddSingleton(_billingFacadeMock.Object);

            _provider = _services.BuildServiceProvider();
            _testSink = _provider.GetRequiredService<ITestLoggerSink>();
        }

        [TestMethod]
        public void TestAckReceivedSuccess()
        {
            var fv = _provider.GetRequiredService<IFileValidator>();
            using var ackFile = fv.GenerateACKFile(Models.AckResultCodes.Success, "test");

            _imspFacadeMock
                .Setup(s => s.GetFile(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(ackFile);

            _imspFacadeMock
                .Setup(s => s.ListIncomingFiles(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(new List<string> { $"{Guid.NewGuid()}-CCEI-ack.xml" });

            var cop = _provider.GetRequiredService<ICOProcessor>();
            cop.Process(1);

            var errors = _testSink.LogEntries.Where(w => w.LogLevel == LogLevel.Error);
            Assert.IsFalse(errors.Any());
        }

        [TestMethod]
        public void TestAckWithCorrectErrorCodeSentWhenChecksumFails()
        {
            var fv = _provider.GetRequiredService<IFileValidator>();
            using var ackFile = fv.GenerateACKFile(Models.AckResultCodes.ChecksumVerificationFailed, "test");

            _imspFacadeMock
                .Setup(s => s.GetFile(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(ackFile);

            _imspFacadeMock
                .Setup(s => s.ListIncomingFiles(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(new List<string> { $"{Guid.NewGuid()}-CCEI-ack.xml" });

            var cop = _provider.GetRequiredService<ICOProcessor>();
            cop.Process(1);

            var errors = _testSink.LogEntries.Where(w => w.LogLevel == LogLevel.Error);
            Assert.IsTrue(errors.Any());
        }

        [TestMethod]
        public void TestCCEIUploadAsZip()
        {
            var fv = _provider.GetRequiredService<IFileValidator>();
            using var zipContentsStream = new MemoryStream();
            using var sw = new StreamWriter(zipContentsStream);
            sw.Write(Guid.NewGuid());
            sw.Flush();
            zipContentsStream.Position = 0;

            using var zipFileStream = fv.Zip(zipContentsStream);

            _imspFacadeMock
                .Setup(s => s.ListIncomingFiles(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(new List<string> { $"{Guid.NewGuid()}-CCEI.zip" });

            _imspFacadeMock
                .Setup(s => s.GetFile(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(zipFileStream.Stream);

            var cop = _provider.GetRequiredService<ICOProcessor>();
            cop.Process(1);

            _imspFacadeMock.Verify(v => v.SendFile(It.IsAny<int>(), It.IsAny<Stream>(), It.IsAny<string>()));
            _billingFacadeMock.Verify(v => v.UploadCCEI(It.IsAny<Stream>(), It.IsAny<int>(), It.IsAny<string>()));

            var errors = _testSink.LogEntries.Any(a => a.LogLevel == LogLevel.Error);
            Assert.IsFalse(errors);
        }

        [TestMethod]
        public void TestCCEIUploadAsXml()
        {
            var fv = _provider.GetRequiredService<IFileValidator>();
            using var xmlContentsStream = new MemoryStream();
            using var sw = new StreamWriter(xmlContentsStream);
            sw.Write(Guid.NewGuid());
            sw.Flush();
            xmlContentsStream.Position = 0;

            _imspFacadeMock
                .Setup(s => s.ListIncomingFiles(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(new List<string> { $"{Guid.NewGuid()}-CCEI.xml" });

            _imspFacadeMock
                .Setup(s => s.GetFile(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(xmlContentsStream);

            var cop = _provider.GetRequiredService<ICOProcessor>();
            cop.Process(1);

            _imspFacadeMock.Verify(v => v.SendFile(It.IsAny<int>(), It.IsAny<Stream>(), It.IsAny<string>()), Times.Never(), "shouldn't be sending a response to IMP if it's a XML file");
            _billingFacadeMock.Verify(v => v.UploadCCEI(It.IsAny<Stream>(), It.IsAny<int>(), It.IsAny<string>()));

            var errors = _testSink.LogEntries.Any(a => a.LogLevel == LogLevel.Error);
            Assert.IsFalse(errors);
        }

        [TestMethod]
        public void TestMalformedCCEIUpload()
        {
            var fv = _provider.GetRequiredService<IFileValidator>();
            using var zipContentsStream = new MemoryStream();
            using var sw = new StreamWriter(zipContentsStream);
            sw.Write(Guid.NewGuid());
            sw.Flush();
            zipContentsStream.Position = 0;

            using var zipFileStream = fv.Zip(zipContentsStream);

            //now replace the CCEI file contents so the checksum fails in future
            using (var zip = new ZipArchive(zipFileStream.Stream, ZipArchiveMode.Update, true))
            {
                var entry = zip.GetEntry(zipFileStream.FileName.Replace(".zip", ".xml"));
                using var fileStream = entry.Open();
                using var sw2 = new StreamWriter(fileStream);
                sw2.Write(Guid.NewGuid());
                sw2.Flush();
            }
            zipFileStream.Stream.Position = 0;

            _imspFacadeMock
                .Setup(s => s.ListIncomingFiles(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(new List<string> { $"{Guid.NewGuid()}-CCEI.zip" });

            _imspFacadeMock
                .Setup(s => s.GetFile(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(zipFileStream.Stream);

            var cop = _provider.GetRequiredService<ICOProcessor>();
            cop.Process(1);

            _imspFacadeMock.Verify(v => v.SendFile(It.IsAny<int>(), It.IsAny<Stream>(), It.IsAny<string>()));
            _billingFacadeMock.Verify(v => v.UploadCCEI(It.IsAny<Stream>(), It.IsAny<int>(), It.IsAny<string>()), Times.Never());

            var errors = _testSink.LogEntries.Any(a => a.LogLevel == LogLevel.Error);
            Assert.IsTrue(errors);
        }

        [TestMethod]
        public async Task TestProcessIncomingCCEIXml_FileNotDeletedWhenFailedToBeUploadedToS3()
        {
            var fv = _provider.GetRequiredService<IFileValidator>();
            using var xmlContentsStream = new MemoryStream();
            using var sw = new StreamWriter(xmlContentsStream);
            sw.Write(Guid.NewGuid());
            sw.Flush();
            xmlContentsStream.Position = 0;

            _imspFacadeMock
                .Setup(s => s.GetFile(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(xmlContentsStream);

            _billingFacadeMock
                .Setup(s => s.UploadCCEI(xmlContentsStream, It.IsAny<int>(), It.IsAny<string>()))
                .Throws(new Exception());

            var cop = _provider.GetRequiredService<ICOProcessor>() as COProcessor;

            try
            {
                await cop.ProcessIncomingCCEIXml(1, "test.xml");
                Assert.Fail();
            }
            catch (Exception ex) when (!(ex is AssertFailedException))
            {
            }

            _imspFacadeMock.Verify(v => v.DeleteFile(It.IsAny<int>(), It.IsAny<string>()), Times.Never());
        }

        [TestMethod]
        public async Task TestProcessIncomingCCEIZip_FileNotDeletedWhenFailedToBeUploadedToS3()
        {
            var fvMock = new Mock<IFileValidator>();
            _services.AddSingleton(fvMock.Object);

            using var provider = _services.BuildServiceProvider();

            using var xmlContentsStream = new MemoryStream();
            using var sw = new StreamWriter(xmlContentsStream);
            sw.Write(Guid.NewGuid());
            sw.Flush();
            xmlContentsStream.Position = 0;

            _imspFacadeMock
                .Setup(s => s.GetFile(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(xmlContentsStream);

            fvMock
                .Setup(s => s.Unzip(It.IsAny<Stream>()))
                .Returns<Stream>(s => s);

            _billingFacadeMock
                .Setup(s => s.UploadCCEI(xmlContentsStream, It.IsAny<int>(), It.IsAny<string>()))
                .Throws(new Exception());

            var cop = provider.GetRequiredService<ICOProcessor>() as COProcessor;

            try
            {
                await cop.ProcessIncomingCCEIZip(1, "test.xml");
                Assert.Fail();
            }
            catch (Exception ex) when (!(ex is AssertFailedException))
            {
            }

            _imspFacadeMock.Verify(v => v.DeleteFile(It.IsAny<int>(), It.IsAny<string>()), Times.Never());
        }
    }
}