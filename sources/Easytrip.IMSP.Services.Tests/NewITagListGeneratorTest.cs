﻿using Amazon.S3;
using Amazon.S3.Model;
using Easytrip.IMSP.Common.Core.Config;
using Easytrip.IMSP.Common.Core.Domain;
using Easytrip.IMSP.Common.Core.Utils;
using Easytrip.IMSP.Facades;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Services.Tests
{
    [TestClass]
    public class NewITagListGeneratorTest
    {
        private IMSPConfig _config;
        private Mock<IAmazonS3> _s3ClientMock;
        private Mock<IIMSPFacade> _imspFacadeMock;
        private Mock<IFileValidator> _fileValidatorMock;
        private ServiceProvider _provider;

        [TestInitialize]
        public void Init()
        {
            var services = new ServiceCollection();
            services.AddLogging();

            _config = new IMSPConfig()
            {
                BucketName = "BucketName",
                XSD = new XSDConfig()
                {
                    WLPath = "XSDs/WhiteListSchema.xml",
                    BLPath = "XSDs/BlackListSchema.xml"
                },
                SFTP = new SFTPConfig
                {
                    OutgoingPath = Guid.NewGuid().ToString(),
                    IncomingPath = Guid.NewGuid().ToString()
                }
            };
            services.AddSingleton(_config);

            _s3ClientMock = new Mock<IAmazonS3>();
            services.AddSingleton(_s3ClientMock.Object);

            _imspFacadeMock = new Mock<IIMSPFacade>();
            services.AddSingleton(_imspFacadeMock.Object);

            services.AddSingleton<IXMLHelper, XMLHelper>();


            _fileValidatorMock = new Mock<IFileValidator>();
            services.AddSingleton(_fileValidatorMock.Object);

            services.AddTransient<ITagListGenerator, NewTagListGenerator>();

            _provider = services.BuildServiceProvider();
        }

        [TestMethod]
        public async Task TestWhitelistGeneration()
        {
            var tagList = new List<VehicleTransponderModel>
            {
                new VehicleTransponderModel
                {
                    Id = 1,
                    ContextMark = "123123123123", //to fail, adding characters not supported by IMSP
                    HexAlias = "123ADF",
                    VehicleRegistration = "123d123",
                    VehicleClass = 1
                }
            };

            _s3ClientMock
                .Setup(s => s.GetObjectAsync(It.IsAny<string>(), "XSDs/WhiteListSchema.xml", default))
                .Returns(Task.FromResult(new GetObjectResponse()
                {
                    ResponseStream = Utils.GetLocalFile(@"XSD\WhiteListSchema.xml")
                }));

            _fileValidatorMock
                .Setup(s => s.Zip(It.IsAny<Stream>(), It.IsAny<string>()))
                .Returns<Stream, string>((stream, str) =>
                {
                    stream.Position = 0;
                    return new Models.ZipInfo(stream, str);
                });

            var tagGenerator = _provider.GetRequiredService<ITagListGenerator>();
            await tagGenerator.GenerateXMLFiles(tagList, Common.Core.Utils.ListTypes.White);

            _imspFacadeMock.Verify(v => v.SendFile(It.IsAny<int>(), It.IsAny<Stream>(), It.Is<string>(i => i.Contains("-WL.xml"))));
        }

        [TestMethod]
        public async Task TestBlacklistGeneration()
        {
            var tagList = new List<VehicleTransponderModel>
            {
                new VehicleTransponderModel
                {
                    Id = 1,
                    ContextMark = "123123123123", //to fail, adding characters not supported by IMSP
                    HexAlias = "123ADF",
                    VehicleRegistration = "123d123",
                    VehicleClass = 1
                }
            };

            _s3ClientMock
                .Setup(s => s.GetObjectAsync(It.IsAny<string>(), "XSDs/BlackListSchema.xml", default))
                .Returns(Task.FromResult(new GetObjectResponse()
                {
                    ResponseStream = Utils.GetLocalFile(@"XSD\BlackListSchema.xml")
                }));

            _fileValidatorMock
                .Setup(s => s.Zip(It.IsAny<Stream>(), It.IsAny<string>()))
                .Returns<Stream, string>((stream, str) =>
                {
                    stream.Position = 0;
                    return new Models.ZipInfo(stream, str);
                });

            var tagGenerator = _provider.GetRequiredService<ITagListGenerator>();
            await tagGenerator.GenerateXMLFiles(tagList, Common.Core.Utils.ListTypes.Black);

            _imspFacadeMock.Verify(v => v.SendFile(It.IsAny<int>(), It.IsAny<Stream>(), It.Is<string>(i => i.Contains("-BL.xml"))));
        }

        [TestMethod]
        public async Task TestGreylistGeneration()
        {
            var tagList = new List<VehicleTransponderModel>
            {
                new VehicleTransponderModel
                {
                    Id = 1,
                    ContextMark = "123123123123", //to fail, adding characters not supported by IMSP
                    HexAlias = "123ADF",
                    VehicleRegistration = "123d123",
                    VehicleClass = 1
                }
            };

            _s3ClientMock
                .Setup(s => s.GetObjectAsync(It.IsAny<string>(), "XSDs/BlackListSchema.xml", default))
                .Returns(Task.FromResult(new GetObjectResponse()
                {
                    ResponseStream = Utils.GetLocalFile(@"XSD\BlackListSchema.xml")
                }));

            _fileValidatorMock
                .Setup(s => s.Zip(It.IsAny<Stream>(), It.IsAny<string>()))
                .Returns<Stream, string>((stream, str) =>
                {
                    stream.Position = 0;
                    return new Models.ZipInfo(stream, str);
                });

            var tagGenerator = _provider.GetRequiredService<ITagListGenerator>();
            await tagGenerator.GenerateXMLFiles(tagList, Common.Core.Utils.ListTypes.Grey);

            _imspFacadeMock.Verify(v => v.SendFile(It.IsAny<int>(), It.IsAny<Stream>(), It.Is<string>(i => i.Contains("-GL.xml"))));
        }

        [TestMethod]
        public async Task TestGeneratedListAlsoWrittenToArchive()
        {
            var tagList = new List<VehicleTransponderModel>
            {
                new VehicleTransponderModel
                {
                    Id = 1,
                    ContextMark = "123123123123", //to fail, adding characters not supported by IMSP
                    HexAlias = "123ADF",
                    VehicleRegistration = "123d123",
                    VehicleClass = 1
                }
            };

            _s3ClientMock
                .Setup(s => s.GetObjectAsync(It.IsAny<string>(), "XSDs/BlackListSchema.xml", default))
                .Returns(Task.FromResult(new GetObjectResponse()
                {
                    ResponseStream = Utils.GetLocalFile(@"XSD\BlackListSchema.xml")
                }));

            _fileValidatorMock
                .Setup(s => s.Zip(It.IsAny<Stream>(), It.IsAny<string>()))
                .Returns<Stream, string>((stream, str) =>
                {
                    stream.Position = 0;
                    return new Models.ZipInfo(stream, str);
                });

            var tagGenerator = _provider.GetRequiredService<ITagListGenerator>();
            await tagGenerator.GenerateXMLFiles(tagList, Common.Core.Utils.ListTypes.Black);

            _s3ClientMock.Verify(v => v.PutObjectAsync(It.IsAny<PutObjectRequest>(), default));
        }
    }
}