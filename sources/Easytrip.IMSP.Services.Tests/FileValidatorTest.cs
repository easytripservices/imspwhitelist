using Easytrip.IMSP.Common.Core.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Easytrip.IMSP.Services.Tests
{
    [TestClass]
    public class FileValidatorTest
    {
        private ServiceProvider _provider;
        private FileValidator _service;

        [TestInitialize]
        public void Init()
        {
            var services = new ServiceCollection();
            services.AddIMSPServices();
            services.AddLogging();

            var config = new ConfigurationBuilder()
                .AddInMemoryCollection()
                .Build();
            services.AddSingleton<IConfiguration>(config);

            services.AddSingleton<IXMLHelper, XMLHelper>();

            _provider = services.BuildServiceProvider();
            _service = _provider.GetRequiredService<IFileValidator>() as FileValidator;
        }

        [TestMethod]
        public void TestUnzip_CCEIFileMissing()
        {
            using var archive = File.OpenRead("Resources\\CCEIMISSING-CCEI.zip");

            try
            {
                _service.Unzip(archive);
                Assert.Fail();
            }
            catch (FileNotFoundException ex)
            {
            }
        }

        [TestMethod]
        public void TestUnzip_ChecksumFileMissing()
        {
            using var archive = File.OpenRead("Resources\\CHECKSUMMISSING-CCEI.zip");

            try
            {
                _service.Unzip(archive);
                Assert.Fail();
            }
            catch (FileNotFoundException ex)
            {
            }
        }

        [TestMethod]
        public void TestUnzip()
        {
            using var archive = File.OpenRead("Resources\\COMPLETE-CCEI.zip");
            using var stream = _service.Unzip(archive);
            Assert.IsTrue(stream.CanRead);
        }

        [TestMethod]
        public void TestValidateChecksum()
        {
            using var ccei = File.OpenRead("Resources\\XXXXXXXX-CCEI.xml");
            using var checksum = File.OpenRead("Resources\\XXXXXXXX-CCEI.checksum.xml");

            var result = _service.ValidateChecksum(ccei, checksum);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestGenerateACK()
        {
            using var ackStream = _service.GenerateACKFile(Models.AckResultCodes.GenericError, "error test");
            using var sr = new StreamReader(ackStream);
            var ackXmlStr = sr.ReadToEnd();

            Assert.IsTrue(ackXmlStr.Contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>"));
            Assert.AreEqual(1, Regex.Matches(ackXmlStr, @"<received_date>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}<\/received_date>").Count);
            Assert.AreEqual(1, Regex.Matches(ackXmlStr, @"<error_code>\-?\d+<\/error_code>").Count);
        }

        [TestMethod]
        public void TestComputeHash()
        {
            var value = "ee1b71ef-92ec-4f0a-95ec-ce5ab2bfda68";

            using (var ms = new MemoryStream())
            using (var sw = new StreamWriter(ms))
            {
                sw.Write(value);
                sw.Flush();
                ms.Position = 0;

                var hash = _service.ComputeHash(ms);
                Assert.AreEqual("8a8d748064a15b703bf763cc0588aa947ec562eb7d617e17168145e51068cc3d2c3af262519b239e9061e22eff32e3aace099d59f1b75dedfb91efd75ba1c05e", hash);
            }
        }

        [TestMethod]
        public void TestZip()
        {
            using var ms = new MemoryStream();
            using var sw = new StreamWriter(ms);
            var cceiContent = Guid.NewGuid().ToString();
            sw.Write(cceiContent);
            sw.Flush();
            ms.Position = 0;

            using var zipInfo = _service.Zip(ms);
            using var cceiStream = _service.Unzip(zipInfo.Stream);

            using var sr = new StreamReader(cceiStream);
            var result = sr.ReadToEnd();

            Assert.AreEqual(result, cceiContent);
        }

        [TestMethod]
        public void TestGenerateChecksum()
        {
            using var ms = new MemoryStream();
            using var sw = new StreamWriter(ms);
            sw.Write(Guid.NewGuid());
            ms.Flush();
            ms.Position = 0;

            var name = Guid.NewGuid().ToString();
            using var checksumStream = _service.GenerateChecksumFile(ms, name);
            checksumStream.Position = 0;
            using var sr = new StreamReader(checksumStream);
            var ackXmlStr = sr.ReadToEnd();

            Assert.IsTrue(ackXmlStr.Contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>"));
            Assert.IsTrue(ackXmlStr.Contains($"<filename>{name}</filename>"));
        }
    }
}