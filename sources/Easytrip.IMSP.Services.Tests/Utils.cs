﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Easytrip.IMSP.Services.Tests
{
    public static class Utils
    {
        public static Stream GetLocalFile(string relativePath)
        {
            return File.OpenRead(Path.Combine(GetWorkingDirectory(), relativePath));
        }

        public static string GetWorkingDirectory()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }
    }
}