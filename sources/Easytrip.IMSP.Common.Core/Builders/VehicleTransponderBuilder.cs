﻿using System;
using Easytrip.Global.Request;
using Easytrip.IMSP.Common.Core.Domain;

namespace Easytrip.IMSP.Common.Core.Builders
{
    public static class VehicleTransponderBuilder
    {
        private const string _apiMessagesUserName = "IMSP.BlackList";

        public static VehicleTransponderModel Mapper(VehicleTransponderResponseModel response)
        {
            try
            {
                var vehicleReg = response.VehicleReg;

                //update VehicleReg in Whitelist to NOT be duplicated in case a transponder is assigned to two vehicle
                if (response.ExpiryDate != null)
                {
                    vehicleReg += "X";
                }

                return new VehicleTransponderModel
                {
                    Id = response.CO.Value,
                    ContextMark = response.ContextMark,
                    HexAlias = response.HexAlias,
                    VehicleClass = response.TollingClass ?? 2,
                    VehicleRegistration = vehicleReg + response.RegSuffix, //2237 - RegSuffixes for specific parties
                    Reason = ""
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static DataRequest<T> ToDataRequest<T>(this T clientMessage)
        {
            return clientMessage.ToDataRequest(Guid.NewGuid());
        }

        public static DataRequest<T> ToDataRequest<T>(this T clientMessage, Guid requestId)
        {
            return new DataRequest<T>()
            {
                RequestId = requestId,
                Data = clientMessage,
                UserName = _apiMessagesUserName,
                ChannelKey = Guid.Parse("929B7FD6-1B3D-4FE7-8C8E-09B91F11264B")
            };
        }
    }
}