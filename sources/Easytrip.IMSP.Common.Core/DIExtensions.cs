﻿using Easytrip.IMSP.Common.Core.Config;
using Easytrip.IMSP.Common.Core.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Easytrip.IMSP.Common.Core
{
    public static class DIExtensions
    {
        public static IServiceCollection AddCommonUtils(this IServiceCollection services)
        {
            services.AddSingleton<IXMLHelper, XMLHelper>();
            services.AddSingleton(sp =>
            {
                return sp
                .GetRequiredService<IConfiguration>()
                .GetSection("IMSP")
                .Get<IMSPConfig>();
            });
            services.AddSingleton(sp =>
            {
                return sp
                .GetRequiredService<IConfiguration>()
                .GetSection("Billing")
                .Get<BillingConfig>();
            });
            return services;
        }
    }
}