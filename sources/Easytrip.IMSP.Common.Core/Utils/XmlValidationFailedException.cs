﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easytrip.IMSP.Common.Core.Utils
{
    public class XmlValidationFailedException : Exception
    {
        public XmlValidationFailedException(string msg, Exception innerException = null) : base(msg, innerException)
        {

        }
    }
}