﻿using Amazon.S3;
using Easytrip.IMSP.Common.Core.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace Easytrip.IMSP.Common.Core.Utils
{
    public interface IXMLHelper
    {
        Stream WriteXmlContents(int coItem, IEnumerable<VehicleTransponderModel> items, string dateTimeStr, ListTypes listType);
        Task<bool> PerformXsdValidation(IAmazonS3 amazonS3, Stream xmlStream, string bucketName, string xsdObjectPath);
    }

    public class XMLHelper : IXMLHelper
    {
        private void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            //XmlSeverityType type = XmlSeverityType.Warning;
            //if (Enum.TryParse("Error", out type))
            //{
            //    if (type == XmlSeverityType.Error) throw new Exception(e.Message);
            //}

            //DS: I don't now who wrote that, but the operation is constant, meaning if this function is ever called, the result would always be "throw an exception". Therefore, removing the above completely. 
            throw new XmlValidationFailedException(e.Message, e.Exception);
        }

        /// <summary>
        /// Writes the data in parameters to a stream returned by this function
        /// </summary>
        /// <param name="coItem">CO number to be present in the file</param>
        /// <param name="items">data to be present in the XML</param>
        /// <param name="dateTimeStr">datetime string which should match the datetime of the filename</param>
        public Stream WriteXmlContents(int coItem, IEnumerable<VehicleTransponderModel> items, string dateTimeStr, ListTypes listType)
        {
            MemoryStream memStream = new MemoryStream();
            var xmlWriterSettings = new XmlWriterSettings()
            {
                Indent = true,
                IndentChars = "\t",
                NewLineOnAttributes = false
            };

            using var writer = XmlWriter.Create(memStream, xmlWriterSettings);

            if (coItem != 8)
            {
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");
            }

            writer.WriteStartElement("data");
            writer.WriteAttributeString("source", coItem.ToString());
            writer.WriteAttributeString("target", "0");

            writer.WriteStartElement("list");
            writer.WriteAttributeString("item_nb", "1");

            writer.WriteStartElement("co");
            writer.WriteAttributeString("id", coItem.ToString());
            writer.WriteAttributeString("constitution", dateTimeStr);

            var coItems = items.Where(i => i.Id == coItem);
            writer.WriteAttributeString("item_nb", coItems.Count().ToString());

            foreach (var item in coItems)
            {
                writer.WriteStartElement("item");
                writer.WriteAttributeString("cm", item.ContextMark?.ToUpper());
                writer.WriteAttributeString("obu", item.HexAlias?.ToUpper());

                if (listType == ListTypes.Black)
                {
                    //as per request from MM, the reason should be "SUSPENDED" only if we have the reg, otherwise it should be the reason coming from SI, alternatively it should be an empty string.
                    string reason = string.IsNullOrWhiteSpace(item.Reason) ? "SUSPENDED" : item.Reason;
                    writer.WriteAttributeString("rs", reason);
                }
                else if (listType == ListTypes.White)
                {
                    writer.WriteAttributeString("vc", item.VehicleClass.ToString());
                    writer.WriteAttributeString("vrn", item.VehicleRegistration);
                }
                else if (listType == ListTypes.Grey)
                {
                    //do nothing
                }
                else
                {
                    throw new ArgumentOutOfRangeException($"Invalid list type: {listType}");
                }

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Flush();

            return memStream;
        }

        public async Task<bool> PerformXsdValidation(IAmazonS3 amazonS3, Stream xmlStream, string bucketName, string xsdObjectPath)
        {
            try
            {
                //retrieve XSD schema
                var getXSDResponse = await amazonS3.GetObjectAsync(bucketName, xsdObjectPath);

                using (Stream xSDResponseStream = getXSDResponse.ResponseStream)
                using (StreamReader xSDstreamReader = new StreamReader(xSDResponseStream))
                using (XmlReader xSDreader = XmlReader.Create(xSDstreamReader))
                {
                    xSDreader.Read();

                    XmlSchemaSet schema = new XmlSchemaSet();
                    schema.Add("", xSDreader);

                    xmlStream.Position = 0;

                    var sr = new StreamReader(xmlStream);
                    using XmlReader rd = XmlReader.Create(sr);
                    XDocument doc = XDocument.Load(rd);
                    doc.Validate(schema, ValidationEventHandler);
                }

                return true;
            }
            catch (XmlValidationFailedException ex)
            {
                return false;
            }
        }
    }
}