﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Common.Core.Config
{
    public class IMSPConfig
    {
        public string BucketName { get; set; }
        public string ObjectPrefix { get; set; }
        public SFTPConfig SFTP { get; set; }
        public XSDConfig XSD { get; set; }
    }
}