﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Common.Core.Config
{
    public class BillingConfig
    {
        public class XsdConfig
        {
            public string Bucket { get; set; }
            public string FilePath { get; set; }
        }

        public string SuccessBucket { get; set; }
        public string ErrorBucket { get; set; }
        public XsdConfig XSD { get; set; }
    }
}