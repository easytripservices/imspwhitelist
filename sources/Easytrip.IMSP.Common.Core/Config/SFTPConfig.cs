﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Common.Core.Config
{
    public class SFTPConfig
    {
        public string Host { get; set; }
        public string OutgoingPath { get; set; }
        public string IncomingPath { get; set; }
        public string SecretName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Dictionary<string, int> COPortMapping { get; set; }
    }
}