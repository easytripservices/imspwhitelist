﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easytrip.IMSP.Common.Core.Config
{
    public class XSDConfig
    {
        public string BLPath { get; set; }
        public string WLPath { get; set; }
    }
}
