﻿using System;

namespace Easytrip.IMSP.Common.Core.Domain
{

    public class VehicleTransponderResponseModel
    {
        public long IdentificationNumber { get; set; }
        public string HexAlias { get; set; }
        public string ContextMark { get; set; }
        public int? TollingClass { get; set; }
        public string VehicleReg { get; set; }

        public int? CO { get; set; }
        public string Reason { get; set; } // reason why is the car/transponder in the gray/black list

        public DateTime? ExpiryDate { get; set; }

        public string RegSuffix { get; set; }
    }
}
