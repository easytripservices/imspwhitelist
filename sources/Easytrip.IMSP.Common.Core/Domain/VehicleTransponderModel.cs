﻿using System;

namespace Easytrip.IMSP.Common.Core.Domain
{
    public class VehicleTransponderModel
    {
        public int Id { get; set; }
        public string HexAlias { get; set; }
        public string ContextMark { get; set; }
        public int VehicleClass { get; set; }
        public string VehicleRegistration { get; set; }

        public string Reason { get; set; }

        public DateTime? ExpiryDate { get; set; }
    }
}
