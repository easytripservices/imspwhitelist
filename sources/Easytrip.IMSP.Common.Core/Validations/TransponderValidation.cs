﻿using Easytrip.Global.Request;
using Easytrip.IMSP.Common.Core.Domain;
using System.Collections.Generic;

namespace Easytrip.IMSP.Common.Core.Validations
{
    public static class TransponderValidation
    {
        public static bool IsVehicleTransponderValid(VehicleTransponderResponseModel item)
        {
            if (!ValidateHexInString(item.HexAlias))
            {
                return false;
            }

            if (!ValidateVehicleClass(item.TollingClass))
            {
                return false;
            }

            return true;
        }

        private static bool ValidateVehicleClass(int? test)
        {
            if (!test.HasValue) //NULL value is fine because VehicleTransponderBuilder.Mapper will set a default value
            {
                return true;
            }

            if (test.Value >= 1 && test.Value <= 8)
            {
                return true;
            }

            return false;
        }

        private static bool ValidateHexInString(string test)
        {
            // For C-style hex notation (0xFF) you can use @"\A\b(0[xX])?[0-9a-fA-F]+\b\Z"
            return System.Text.RegularExpressions.Regex.IsMatch(test, @"\A\b[0-9a-fA-F]+\b\Z");
        }
    }
}