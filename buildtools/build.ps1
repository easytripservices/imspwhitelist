$solutionToBuild="$PSScriptRoot\..\Sources\Easytrip.IMSP.sln"
$nugetConfig="$PSScriptRoot\nuget.config"

dotnet clean --configuration Release $solutionToBuild 

dotnet restore --configfile $nugetConfig $solutionToBuild 

dotnet build --configuration Release $solutionToBuild

