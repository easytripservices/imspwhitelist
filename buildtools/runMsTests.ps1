$solutionToBuild="$PSScriptRoot\..\Sources\Easytrip.IMSP.sln"
$testResultsDir="$PSScriptRoot\TestResults"

If (Test-Path $testResultsDir){
	Remove-Item $testResultsDir -Recurse -Force
}

dotnet test --configuration Release --logger trx --results-directory $testResultsDir $solutionToBuild
