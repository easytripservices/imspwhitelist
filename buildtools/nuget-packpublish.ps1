$isBuildServer = ($env:BUILD_NUMBER)

#check paths and variables....
if ($isBuildServer)
{
    cd buildtools
	Write-Output "BUILD_NUMBER variable has value. Paths and defaults will be applied for build server"
	$buildNumber = $env:BUILD_NUMBER
    $destinationPackagesFolder = "D:\ProGet\Pickup\Deployables"
}
else
{
	Write-Output "BUILD_NUMBER variable is empty. Paths and defaults will be applied for local build. Build number applied will be 0"
	$buildNumber = 100
    $destinationPackagesFolder = "C:\LocalNugetRepository\Deployables"
}

Write-output "Installing Amazon Lambda tooling..."
dotnet new tool-manifest --force
dotnet tool install --local --version 5.4.0.0 Amazon.Lambda.Tools 

#Create destination folder
If (!(Test-Path $destinationPackagesFolder)) {
   New-Item -Path $destinationPackagesFolder -ItemType Directory
}

Write-Output "Packaging nuget files to '$destinationPackagesFolder' ..."

#package lambda
dotnet lambda package --configuration Release --project-location "$PSScriptRoot\..\Sources\Easytrip.IMSP.ListTransfer.Lambda"
dotnet pack --no-build --output $destinationPackagesFolder --configuration Release -p:NuspecProperties="Version=1.0.0.$buildNumber" "$PSScriptRoot\..\Sources\Easytrip.IMSP.ListTransfer.Lambda\Easytrip.IMSP.ListTransfer.Lambda.csproj"

#package dbup
dotnet publish --self-contained --configuration Release --runtime win-x64 "$PSScriptRoot\..\Sources\Easytrip.IMSP.FileUtil\Easytrip.IMSP.FileUtil.csproj"
dotnet pack --output $destinationPackagesFolder --configuration Release -p:NuspecProperties="Version=1.0.0.$buildNumber" "$PSScriptRoot\..\Sources\Easytrip.IMSP.FileUtil\Easytrip.IMSP.FileUtil.csproj"
